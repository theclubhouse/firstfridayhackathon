/**
 * jQuery to power lightbox preview.
 * 
 * @package   Tgmsp-Lightbox
 * @version   1.0.0
 * @author    Thomas Griffin <thomas@thomasgriffinmedia.com>
 * @copyright Copyright (c) 2012, Thomas Griffin
 */
jQuery(document).ready(function($) {

	/** Append information to the global settings ID var */
	if ( 'undefined' == typeof soliloquyPreviewSettingsID )
		soliloquyPreviewSettingsID = '#soliloquy_lightbox_settings .form-table td,';
	else
		soliloquyPreviewSettingsID += '#soliloquy_lightbox_settings .form-table td,';

});
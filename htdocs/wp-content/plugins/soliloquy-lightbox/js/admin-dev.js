/**
 * jQuery to power lightbox customizations.
 *
 * The object passed to this script file via wp_localize_script is
 * soliloquy_lightbox. 
 *
 * @package   Tgmsp-Lightbox
 * @version   1.0.0
 * @author    Thomas Griffin <thomas@thomasgriffinmedia.com>
 * @copyright Copyright (c) 2012, Thomas Griffin
 */
jQuery(document).ready(function($) {

	/** Set defaults for fields */
	if ( 0 == $('#soliloquy-lightbox-slideshow-speed').val().length ) {
		$('#soliloquy-lightbox-slideshow-speed').val(soliloquy_lightbox.speed);
	}
	
	if ( 0 == $('#soliloquy-lightbox-opacity').val().length ) {
		$('#soliloquy-lightbox-opacity').val(soliloquy_lightbox.opacity);
	}
	
	if ( 0 == $('#soliloquy-lightbox-width').val().length ) {
		$('#soliloquy-lightbox-width').val(soliloquy_lightbox.width);
	}
	
	if ( 0 == $('#soliloquy-lightbox-height').val().length ) {
		$('#soliloquy-lightbox-height').val(soliloquy_lightbox.height);
	}
	
	if ( 0 == $('#soliloquy-lightbox-separator').val().length ) {
		$('#soliloquy-lightbox-separator').val(soliloquy_lightbox.separator);
	}
	
	if ( 0 == $('#soliloquy-lightbox-padding').val().length ) {
		$('#soliloquy-lightbox-padding').val(soliloquy_lightbox.padding);
	}
	
	if ( 0 == $('#soliloquy-lightbox-wmode').val().length ) {
		$('#soliloquy-lightbox-wmode').val(soliloquy_lightbox.wmode);
	}
	
	if ( $('#soliloquy-lightbox-gallery').is(':checked') ) {
		$('#soliloquy-lightbox-slideshow-box').show();
		if ( $('#soliloquy-lightbox-slideshow').is(':checked') )
			$('#soliloquy-lightbox-slideshow-speed-box').show();
	} else {
		$('#soliloquy-lightbox-slideshow-box, #soliloquy-lightbox-slideshow-speed-box').hide();
	}
	
	if ( $('#soliloquy-lightbox-gallery').is(':checked') && $('#soliloquy-lightbox-slideshow').is(':checked') ) {
		$('#soliloquy-lightbox-slideshow-speed-box').show();
	} else {
		$('#soliloquy-lightbox-slideshow-speed-box').hide();
	}
	
	$('#soliloquy-lightbox-gallery').on('change', function() {
		if ( $(this).is(':checked') ) {
			$('#soliloquy-lightbox-slideshow-box').fadeIn('normal');
			if ( $('#soliloquy-lightbox-slideshow').is(':checked') )
				$('#soliloquy-lightbox-slideshow-speed-box').fadeIn('normal');
		} else {
			$('#soliloquy-lightbox-slideshow-box, #soliloquy-lightbox-slideshow-speed-box').fadeOut('normal');
		}
	});
	
	$('#soliloquy-lightbox-slideshow').on('change', function() {
		if ( $(this).is(':checked') ) {
			$('#soliloquy-lightbox-slideshow-speed-box').fadeIn('normal');
		} else {
			$('#soliloquy-lightbox-slideshow-speed-box').fadeOut('normal');
		}
	});
	
		/** Hide advanced options on page load */
	if ( $('#soliloquy-lightbox-advanced').is(':checked') ) {
		$('#soliloquy-lightbox-gallery-box, #soliloquy-lightbox-title-box, #soliloquy-lightbox-resize-box, #soliloquy-lightbox-default-size-box, #soliloquy-lightbox-separator-box, #soliloquy-lightbox-padding-box, #soliloquy-lightbox-flash-box, #soliloquy-lightbox-wmode-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-modal-box, #soliloquy-lightbox-deeplinking-box, #soliloquy-lightbox-overlay-box, #soliloquy-lightbox-keyboard-box, #soliloquy-lightbox-ie6-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-social-box, .soliloquy-lightbox-advanced').show();
		if ( $('#soliloquy-lightbox-gallery').is(':checked') ) {
			$('#soliloquy-lightbox-slideshow-box').show();
			if ( $('#soliloquy-lightbox-slideshow').is(':checked') )
				$('#soliloquy-lightbox-slideshow-speed-box').show();
		} else {
			$('#soliloquy-lightbox-slideshow-box, #soliloquy-lightbox-slideshow-speed-box').hide();
		}
	
		if ( $('#soliloquy-lightbox-gallery').is(':checked') && $('#soliloquy-lightbox-slideshow').is(':checked') ) {
			$('#soliloquy-lightbox-slideshow-speed-box').show();
		} else {
			$('#soliloquy-lightbox-slideshow-speed-box').hide();
		}
	} else {
		$('#soliloquy-lightbox-gallery-box, #soliloquy-lightbox-slideshow-box, #soliloquy-lightbox-slideshow-speed-box, #soliloquy-lightbox-title-box, #soliloquy-lightbox-resize-box, #soliloquy-lightbox-default-size-box, #soliloquy-lightbox-separator-box, #soliloquy-lightbox-padding-box, #soliloquy-lightbox-flash-box, #soliloquy-lightbox-wmode-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-modal-box, #soliloquy-lightbox-deeplinking-box, #soliloquy-lightbox-overlay-box, #soliloquy-lightbox-keyboard-box, #soliloquy-lightbox-ie6-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-social-box, #soliloquy-lightbox-facebook-box, #soliloquy-lightbox-twitter-box, .soliloquy-lightbox-advanced').hide();
	}
	
	$('#soliloquy-lightbox-advanced').on('change', function() {
		if ( $(this).is(':checked') ) {
			$('#soliloquy-lightbox-gallery-box, #soliloquy-lightbox-title-box, #soliloquy-lightbox-resize-box, #soliloquy-lightbox-default-size-box, #soliloquy-lightbox-separator-box, #soliloquy-lightbox-padding-box, #soliloquy-lightbox-flash-box, #soliloquy-lightbox-wmode-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-modal-box, #soliloquy-lightbox-deeplinking-box, #soliloquy-lightbox-overlay-box, #soliloquy-lightbox-keyboard-box, #soliloquy-lightbox-ie6-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-social-box, .soliloquy-lightbox-advanced').fadeIn('normal');
			if ( $('#soliloquy-lightbox-gallery').is(':checked') ) {
				$('#soliloquy-lightbox-slideshow-box').fadeIn();
				if ( $('#soliloquy-lightbox-slideshow').is(':checked') ) {
					$('#soliloquy-lightbox-slideshow-speed-box').fadeIn();
				}
			}
		} else {
			$('#soliloquy-lightbox-gallery-box, #soliloquy-lightbox-slideshow-box, #soliloquy-lightbox-slideshow-speed-box, #soliloquy-lightbox-title-box, #soliloquy-lightbox-resize-box, #soliloquy-lightbox-default-size-box, #soliloquy-lightbox-separator-box, #soliloquy-lightbox-padding-box, #soliloquy-lightbox-flash-box, #soliloquy-lightbox-wmode-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-modal-box, #soliloquy-lightbox-deeplinking-box, #soliloquy-lightbox-overlay-box, #soliloquy-lightbox-keyboard-box, #soliloquy-lightbox-ie6-box, #soliloquy-lightbox-autoplay-box, #soliloquy-lightbox-social-box, #soliloquy-lightbox-facebook-box, #soliloquy-lightbox-twitter-box, .soliloquy-lightbox-advanced').fadeOut('normal');
		}
	});
	
	/** Set defaults for lightbox image meta fields */
	$('#soliloquy-area').find('.soliloquy-lightbox-flash-width').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.width);
		}
	});
	$('#soliloquy-area').find('.soliloquy-lightbox-flash-height').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.height);
		}
	});
	
	$('#soliloquy-area').find('.soliloquy-lightbox-qt-width').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.width);
		}
	});
	$('#soliloquy-area').find('.soliloquy-lightbox-qt-height').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.height);
		}
	});
	
	$('#soliloquy-area').find('.soliloquy-lightbox-iframe-width').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.width);
		}
	});
	$('#soliloquy-area').find('.soliloquy-lightbox-iframe-height').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.height);
		}
	});
	
	$('#soliloquy-area').find('.soliloquy-lightbox-ajax-width').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.width);
		}
	});
	$('#soliloquy-area').find('.soliloquy-lightbox-ajax-height').each(function() {
		if ( 0 == $(this).val().length || 0 == $(this).val() ) {
			$(this).val(soliloquy_lightbox.height);
		}
	});

	/** Show/Hide lightbox options */
	$(document).find('.soliloquy-lightbox').each(function() {
		if ( $(this).is(':checked') ) {
			$(this).parent().parent().next().show();
		} else {
			$(this).parent().parent().next().hide();
		}
	});
	
	$(document).find('.soliloquy-lightbox-content').each(function() {
		var content_message = $(this);
		var content_type 	= $(this).val();
		soliloquyLightboxContentDisplay(content_message, content_type, 'show');
	});
	
	$('#soliloquy-images').on('ajaxStop', function() {
		$(this).find('.soliloquy-lightbox').each(function() {
			if ( $(this).is(':checked') ) {
				$(this).parent().parent().next().show();
			} else {
				$(this).parent().parent().next().hide();
			}
		});
		
		$(this).find('.soliloquy-lightbox-content').each(function() {
			var content_message = $(this);
			var content_type 	= $(this).val();
			soliloquyLightboxContentDisplay(content_message, content_type, 'show');
		});
		
		$(this).find('.soliloquy-lightbox-flash-width').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.width);
			}
		});
		$(this).find('.soliloquy-lightbox-flash-height').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.height);
			}
		});
	
		$(this).find('.soliloquy-lightbox-qt-width').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.width);
			}
		});
		$(this).find('.soliloquy-lightbox-qt-height').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.height);
			}
		});
	
		$(this).find('.soliloquy-lightbox-iframe-width').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.width);
			}
		});
		$(this).find('.soliloquy-lightbox-iframe-height').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.height);
			}
		});
	
		$(this).find('.soliloquy-lightbox-ajax-width').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.width);
			}
		});
		$(this).find('.soliloquy-lightbox-ajax-height').each(function() {
			if ( 0 == $(this).val().length || 0 == $(this).val() ) {
				$(this).val(soliloquy_lightbox.height);
			}
		});
	});
	
	$(document).on('change.selectSoliloquyLightbox', '.soliloquy-lightbox', function() {
		if ( $(this).is(':checked') ) {
			$(this).parent().parent().next().fadeIn('normal');
		} else {
			$(this).parent().parent().next().fadeOut('normal');
		}
	});
	
	$(document).on('change.selectSoliloquyLightboxContentType', '.soliloquy-lightbox-content', function() {
		var content_message = $(this);
		var content_type 	= $(this).val();
		soliloquyLightboxContentDisplay(content_message, content_type, 'fade');
	})
	
	/** Callback function for displaying lightbox content messages */
	function soliloquyLightboxContentDisplay(message, type, action) {
		if ( 'show' == action ) {
			switch ( type ) {
				case 'image' :
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'flash' :
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'quicktime' :
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'iframe' :
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'inline' :
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'ajax' :
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').show();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					break;
				default :
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
			}
		} else if ( 'fade' == action ) {
			switch ( type ) {
				case 'image' :
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'flash' :
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'quicktime' :
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'iframe' :
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'inline' :
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
				case 'ajax' :
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').fadeIn();
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					break;
				default :
					$(message).parent().parent().find('#soliloquy-lightbox-image-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-flash-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-qt-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-iframe-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-inline-desc').hide();
					$(message).parent().parent().find('#soliloquy-lightbox-ajax-desc').hide();
					break;
			}
		}
	}

});
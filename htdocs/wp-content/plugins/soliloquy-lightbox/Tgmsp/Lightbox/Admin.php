<?php
/**
 * Admin class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Admin {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		add_action( 'admin_init', array( $this, 'deactivation' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_lightbox_settings' ), 10, 2 );

	}

	/**
	 * Deactivate the plugin if Soliloquy is not active and update the recently
	 * activate plugins with our plugin.
	 *
	 * @since 1.0.0
	 */
	public function deactivation() {

		/** Don't deactivate when doing a Soliloquy update or when editing Soliloquy from the Plugin Editor */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() ) {
			$recent = (array) get_option( 'recently_activated' );
			$recent[plugin_basename( Tgmsp_Lightbox::get_file() )] = time();
			update_option( 'recently_activated', $recent );
			deactivate_plugins( plugin_basename( Tgmsp_Lightbox::get_file() ) );
		}

	}

	/**
	 * Adds the Soliloquy lightbox metabox to the Soliloquy edit screen.
	 *
	 * @since 1.0.0
	 */
	public function add_meta_boxes() {

		add_meta_box( 'soliloquy_lightbox_settings', Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_settings'], array( $this, 'lightbox_settings' ), 'soliloquy', 'normal', 'core' );

	}

	/**
	 * Callback function for the Soliloquy lightbox metabox.
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object
	 */
	public function lightbox_settings( $post ) {

		/** Always keep security first */
		wp_nonce_field( 'soliloquy_lightbox_settings', 'soliloquy_lightbox_settings' );

		do_action( 'tgmsp_lightbox_before_settings_table', $post );

		?>
		<p><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['settings_desc']; ?></p>
		<table class="form-table">
			<tbody>
			    <?php do_action( 'tgmsp_lightbox_before_setting_auto', $post ); ?>
				<tr id="soliloquy-lightbox-auto-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-auto"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['auto']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-auto" type="checkbox" name="_soliloquy_lightbox[auto]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'auto' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'auto' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['auto_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_theme', $post ); ?>
				<tr id="soliloquy-lightbox-theme-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-theme"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['theme']; ?></label></th>
					<td>
					<?php
						$themes = $this->themes();
						echo '<select id="soliloquy-lightbox-theme" name="_soliloquy_lightbox[lightbox_theme]">';
						foreach ( (array) $themes as $array => $data )
							echo '<option value="' . esc_attr( $data['type'] ) . '"' . selected( $data['type'], Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_theme' ), false ) . '>' . esc_html( $data['name'] ) . '</option>';
						echo '</select>';
					?>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_animation', $post ); ?>
				<tr id="soliloquy-lightbox-animation-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-animation"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['animation']; ?></label></th>
					<td>
					<?php
						$animations = apply_filters( 'tgmsp_lightbox_default_animation', array( 'fast', 'normal', 'slow' ) );
						echo '<select id="soliloquy-lightbox-animation" name="_soliloquy_lightbox[animation]">';
						foreach ( (array) $animations as $animation )
							echo '<option value="' . esc_attr( $animation ) . '"' . selected( $animation, Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'animation' ), false ) . '>' . esc_html( $animation ) . '</option>';
						echo '</select>';
					?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['animation_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_opacity', $post ); ?>
				<tr id="soliloquy-lightbox-opacity-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-opacity"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['opacity']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-opacity" type="text" name="_soliloquy_lightbox[opacity]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'opacity' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['opacity_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_advanced', $post ); ?>
				<tr id="soliloquy-lightbox-advanced-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-advanced"><strong><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['advanced']; ?></strong></label></th>
					<td>
						<input id="soliloquy-lightbox-advanced" type="checkbox" name="_soliloquy_lightbox[advanced]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'advanced' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'advanced' ), 1 ); ?> />
						<span class="description"><strong><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['advanced_desc']; ?></strong></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_gallery', $post ); ?>
				<tr id="soliloquy-lightbox-gallery-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-gallery"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['gallery']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_gallery' ) ) { ?>
							<input id="soliloquy-lightbox-gallery" type="checkbox" name="_soliloquy_lightbox[lightbox_gallery]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-gallery" type="checkbox" name="_soliloquy_lightbox[lightbox_gallery]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_gallery' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_gallery' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['gallery_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_slideshow', $post ); ?>
				<tr id="soliloquy-lightbox-slideshow-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-slideshow"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['slideshow']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-slideshow" type="checkbox" name="_soliloquy_lightbox[lightbox_slideshow]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_slideshow' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_slideshow' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['slideshow_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_slideshow_speed', $post ); ?>
				<tr id="soliloquy-lightbox-slideshow-speed-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-slideshow-speed"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['speed']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-slideshow-speed" type="text" name="_soliloquy_lightbox[lightbox_slideshow_speed]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_slideshow_speed' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['speed_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_title', $post ); ?>
				<tr id="soliloquy-lightbox-title-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-title"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['title']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'title' ) ) { ?>
							<input id="soliloquy-lightbox-title" type="checkbox" name="_soliloquy_lightbox[title]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-title" type="checkbox" name="_soliloquy_lightbox[title]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'title' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'title' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['title_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_resize', $post ); ?>
				<tr id="soliloquy-lightbox-resize-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-resize"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['resize']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'resize' ) ) { ?>
							<input id="soliloquy-lightbox-resize" type="checkbox" name="_soliloquy_lightbox[resize]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-resize" type="checkbox" name="_soliloquy_lightbox[resize]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'resize' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'resize' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['resize_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_default_size', $post ); ?>
				<tr id="soliloquy-lightbox-default-size-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-width"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['default_size']; ?></label></th>
					<td>
						<div id="soliloquy-lightbox-default-sizes">
							<input id="soliloquy-lightbox-width" type="text" name="_soliloquy_lightbox[width]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'width' ) ); ?>" /> &#215; <input id="soliloquy-lightbox-height" type="text" name="_soliloquy_lightbox[height]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'height' ) ); ?>" />
							<p class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['default_size_desc']; ?></p>
						</div>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_separator', $post ); ?>
				<tr id="soliloquy-lightbox-separator-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-separator"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['separator']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-separator" type="text" name="_soliloquy_lightbox[separator]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'separator' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['separator_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_padding', $post ); ?>
				<tr id="soliloquy-lightbox-padding-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-padding"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['padding']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-padding" type="text" name="_soliloquy_lightbox[padding]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'padding' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['padding_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_flash', $post ); ?>
				<tr id="soliloquy-lightbox-flash-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-flash"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['flash']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-flash" type="checkbox" name="_soliloquy_lightbox[flash]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'flash' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'lightbox_gallery' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['flash_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_wmode', $post ); ?>
				<tr id="soliloquy-lightbox-wmode-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-wmode"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['wmode']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-wmode" type="text" name="_soliloquy_lightbox[wmode]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'wmode' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['wmode_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_autoplay', $post ); ?>
				<tr id="soliloquy-lightbox-autoplay-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-autoplay"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['autoplay']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'autoplay' ) ) { ?>
							<input id="soliloquy-lightbox-autoplay" type="checkbox" name="_soliloquy_lightbox[autoplay]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-autoplay" type="checkbox" name="_soliloquy_lightbox[autoplay]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'autoplay' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'autoplay' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['autoplay_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_modal', $post ); ?>
				<tr id="soliloquy-lightbox-modal-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-modal"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['modal']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-modal" type="checkbox" name="_soliloquy_lightbox[modal]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'modal' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'modal' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['modal_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_deeplinking', $post ); ?>
				<tr id="soliloquy-lightbox-deeplinking-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-deeplinking"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['deeplinking']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'deeplinking' ) ) { ?>
							<input id="soliloquy-lightbox-deeplinking" type="checkbox" name="_soliloquy_lightbox[deeplinking]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-deeplinking" type="checkbox" name="_soliloquy_lightbox[deeplinking]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'deeplinking' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'deeplinking' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['deeplinking_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_overlay', $post ); ?>
				<tr id="soliloquy-lightbox-overlay-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-overlay"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['overlay']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'overlay' ) ) { ?>
							<input id="soliloquy-lightbox-overlay" type="checkbox" name="_soliloquy_lightbox[overlay]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-overlay" type="checkbox" name="_soliloquy_lightbox[overlay]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'overlay' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'overlay' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['overlay_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_keyboard', $post ); ?>
				<tr id="soliloquy-lightbox-keyboard-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-keyboard"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['keyboard']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'keyboard' ) ) { ?>
							<input id="soliloquy-lightbox-keyboard" type="checkbox" name="_soliloquy_lightbox[keyboard]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-keyboard" type="checkbox" name="_soliloquy_lightbox[keyboard]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'keyboard' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'keyboard' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['keyboard_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_ie6', $post ); ?>
				<tr id="soliloquy-lightbox-ie6-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-ie6"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['ie6']; ?></label></th>
					<td>
					<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'ie6' ) ) { ?>
							<input id="soliloquy-lightbox-ie6" type="checkbox" name="_soliloquy_lightbox[ie6]" value="1" checked="checked" /> <?php } else { ?>
							<input id="soliloquy-lightbox-ie6" type="checkbox" name="_soliloquy_lightbox[ie6]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'ie6' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'ie6' ), 1 ); ?> /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['ie6_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_before_setting_social', $post ); ?>
				<tr id="soliloquy-lightbox-social-box" valign="middle">
					<th scope="row"><label for="soliloquy-lightbox-social"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['social']; ?></label></th>
					<td>
						<input id="soliloquy-lightbox-social" type="checkbox" name="_soliloquy_lightbox[social]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'social' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_lightbox', 'social' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['social_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_lightbox_end_of_settings', $post ); ?>
			</tbody>
		</table>

		<?php do_action( 'tgmsp_lightbox_after_settings_table', $post ); ?>

		<div class="soliloquy-lightbox-advanced">
			<p><strong><?php echo Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_cb']; ?></strong></p>
		</div>
		<?php

		do_action( 'tgmsp_lightbox_after_settings', $post );

	}

	/**
	 * Save lightbox settings post meta fields added to Soliloquy metaboxes.
	 *
	 * @since 1.0.0
	 *
	 * @param int $post_id The post ID
	 * @param object $post Current post object data
	 */
	public function save_lightbox_settings( $post_id, $post ) {

		/** Bail out if we fail a security check */
		if ( ! isset( $_POST[sanitize_key( 'soliloquy_lightbox_settings' )] ) || ! wp_verify_nonce( $_POST[sanitize_key( 'soliloquy_lightbox_settings' )], 'soliloquy_lightbox_settings' ) )
			return $post_id;

		/** Bail out if running an autosave, ajax or a cron */
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			return;
		if ( defined( 'DOING_CRON' ) && DOING_CRON )
			return;

		/** Bail out if the user doesn't have the correct permissions to update the slider */
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;

		/** All security checks passed, so let's store our data */
		$settings = isset( $_POST['_soliloquy_lightbox'] ) ? $_POST['_soliloquy_lightbox'] : '';

		/** Sanitize all data before updating */
		$settings['auto'] 					    = isset( $_POST['_soliloquy_lightbox']['auto'] ) ? 1 : 0;
		$opacity 								= floatval( $_POST['_soliloquy_lightbox']['opacity'] ) ? number_format( floatval( $_POST['_soliloquy_lightbox']['opacity'] ), 2 ) : .80;
		if ( $opacity > 1 )
			$opacity 							= .80;
		$settings['opacity'] 					= floatval( $opacity );
		$settings['lightbox_theme'] 			= preg_replace( '#[^a-z0-9-_]#', '', $_POST['_soliloquy_lightbox']['lightbox_theme'] );
		$settings['animation'] 					= preg_replace( '#[^a-z0-9-_]#', '', $_POST['_soliloquy_lightbox']['animation'] );
		$settings['advanced'] 					= isset( $_POST['_soliloquy_lightbox']['advanced'] ) ? 1 : 0;
		$settings['lightbox_gallery'] 			= isset( $_POST['_soliloquy_lightbox']['lightbox_gallery'] ) ? 1 : 0;
		$settings['lightbox_slideshow'] 		= isset( $_POST['_soliloquy_lightbox']['lightbox_slideshow'] ) ? 1 : 0;
		$settings['lightbox_slideshow_speed'] 	= absint( $_POST['_soliloquy_lightbox']['lightbox_slideshow_speed'] ) ? absint( $_POST['_soliloquy_lightbox']['lightbox_slideshow_speed'] ) : 5000;
		$settings['title'] 						= isset( $_POST['_soliloquy_lightbox']['title'] ) ? 1 : 0;
		$settings['width']						= absint( $_POST['_soliloquy_lightbox']['width'] ) ? absint( $_POST['_soliloquy_lightbox']['width'] ) : 500;
		$settings['height']						= absint( $_POST['_soliloquy_lightbox']['height'] ) ? absint( $_POST['_soliloquy_lightbox']['height'] ) : 344;
		$settings['separator'] 					= esc_attr( $_POST['_soliloquy_lightbox']['separator'] );
		$settings['padding']					= absint( $_POST['_soliloquy_lightbox']['padding'] ) ? absint( $_POST['_soliloquy_lightbox']['padding'] ) : 20;
		$settings['flash'] 						= isset( $_POST['_soliloquy_lightbox']['flash'] ) ? 1 : 0;
		$settings['wmode'] 						= esc_attr( $_POST['_soliloquy_lightbox']['wmode'] );
		$settings['autoplay'] 					= isset( $_POST['_soliloquy_lightbox']['autoplay'] ) ? 1 : 0;
		$settings['modal'] 						= isset( $_POST['_soliloquy_lightbox']['modal'] ) ? 1 : 0;
		$settings['deeplinking'] 				= isset( $_POST['_soliloquy_lightbox']['deeplinking'] ) ? 1 : 0;
		$settings['overlay'] 					= isset( $_POST['_soliloquy_lightbox']['overlay'] ) ? 1 : 0;
		$settings['keyboard'] 					= isset( $_POST['_soliloquy_lightbox']['keyboard'] ) ? 1 : 0;
		$settings['ie6'] 						= isset( $_POST['_soliloquy_lightbox']['ie6'] ) ? 1 : 0;
		$settings['social'] 					= isset( $_POST['_soliloquy_lightbox']['social'] ) ? 1 : 0;

		do_action( 'tgmsp_lightbox_save_settings', $settings, $post_id, $post );

		/** Update post meta with sanitized values */
		update_post_meta( $post_id, '_soliloquy_lightbox', $settings );

	}

	/**
	 * Default lightbox themes that can be filtered.
	 *
	 * @since 1.0.0
	 */
	public function themes() {

		$themes = array(
			array(
				'type'	=> 'pp_default',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_pp_default']
			),
			array(
				'type'	=> 'light_rounded',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_light_rounded']
			),
			array(
				'type'	=> 'dark_rounded',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_dark_rounded']
			),
			array(
				'type'	=> 'light_square',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_light_square']
			),
			array(
				'type'	=> 'dark_square',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_dark_square']
			),
			array(
				'type'	=> 'facebook',
				'name'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['theme_facebook']
			)
		);

		return apply_filters( 'tgmsp_lightbox_default_themes', $themes );

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
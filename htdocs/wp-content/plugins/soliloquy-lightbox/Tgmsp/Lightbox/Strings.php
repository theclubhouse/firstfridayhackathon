<?php
/**
 * Strings class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Strings {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Holds a copy of all the strings used by the Soliloquy Lightbox Addon.
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	public $strings = array();

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;

		$this->strings = apply_filters( 'tgmsp_lightbox_strings', array(
			'advanced'					=> __( 'Show Advanced Options?', 'soliloquy-lightbox' ),
			'advanced_desc'				=> __( 'Check this box to show more advanced settings for this slider.', 'soliloquy-lightbox' ),
			'advanced_help'				=> __( 'Soliloquy Lightbox', 'soliloquy-lightbox' ),
			'advanced_help_desc'		=> __( 'Each lightbox comes with dynamic action hooks to supply custom callback parameters using prettyPhoto\'s API. The callback parameters and related action hooks for this lightbox are listed below.', 'soliloquy-lightbox' ),
			'ajax_height'				=> __( 'Ajax Height', 'soliloquy-lightbox' ),
			'ajax_width'				=> __( 'Ajax Width', 'soliloquy-lightbox' ),
			'animation'					=> __( 'Animation Speed', 'soliloquy-lightbox' ),
			'animation_desc'			=> __( 'Set the animation speed during transitions.', 'soliloquy-lightbox' ),
			'auto'                      => __( 'Autolink images to load in lightbox?', 'soliloquy-lightbox' ),
			'auto_desc'                 => __( 'Automatically links all images to load in lightbox when clicked.', 'soliloquy-lightbox' ),
			'autoplay'					=> __( 'Autoplay Videos?', 'soliloquy-lightbox' ),
			'autoplay_desc'				=> __( 'Autoplay videos when opened in lightbox or not.', 'soliloquy-lightbox' ),
			'content_settings'			=> __( 'The width and height settings above can be percentage based (e.g. 100%).', 'soliloquy-lightbox' ),
			'content_settings_flash'	=> __( 'Your URL must point to the .swf file in order for lightbox to grab and display it correctly.', 'soliloquy-lightbox' ),
			'content_settings_qt'		=> __( 'Your URL must point to the .mov file in order for lightbox to grab and display it correctly.', 'soliloquy-lightbox' ),
			'deeplinking'				=> __( 'Allow Deeplinking?', 'soliloquy-lightbox' ),
			'deeplinking_desc'			=> __( 'Allow prettyPhoto to update the URL to enable deeplinking.', 'soliloquy-lightbox' ),
			'default_size'				=> __( 'Default Size of Lightbox', 'soliloquy-lightbox' ),
			'default_size_desc'			=> __( 'The width and height of the lightbox overlay.', 'soliloquy-lightbox' ),
			'enable_lightbox'			=> __( 'Enable lightbox?', 'soliloquy-lightbox' ),
			'enable_lightbox_desc'		=> __( 'Even if checked, no lightbox will display if no URL is set.', 'soliloquy-lightbox' ),
			'flash'						=> __( 'Hide Flash Objects?', 'soliloquy-lightbox' ),
			'flash_desc'				=> __( 'Check this box if Flash objects on your page are appearing on top of the lightbox overlay.', 'soliloquy-lightbox' ),
			'flash_height'				=> __( 'Flash Height', 'soliloquy-lightbox' ),
			'flash_width'				=> __( 'Flash Width', 'soliloquy-lightbox' ),
			'image_video_link'			=> __( 'image or video link', 'soliloquy-lightbox' ),
			'gallery'					=> __( 'Use Lightbox as Image Gallery?', 'soliloquy-lightbox' ),
			'gallery_desc'				=> __( 'Select this option to display all lightbox content in this slider as a gallery.', 'soliloquy-lightbox' ),
			'ie6'						=> __( 'IE6 Fallback Support?', 'soliloquy-lightbox' ),
			'ie6_desc'					=> __( 'Gracefully degrade lightbox for IE6.', 'soliloquy-lightbox' ),
			'iframe_height'				=> __( 'Iframe Height', 'soliloquy-lightbox' ),
			'iframe_width'				=> __( 'Iframe Width', 'soliloquy-lightbox' ),
			'inline_desc_one'			=> __( 'When opening inline content within your lightbox, point the image link to the ID tag of your content. <strong>The inline content must be delivered from the same page as lightbox.</strong> Look at the URL below to see an example of formatting.', 'soliloquy-lightbox' ),
			'inline_desc_two'			=> __( '#my-inline-selector', 'soliloquy-lightbox' ),
			'inline_desc_three'			=> __( 'This would grab content from an element with an ID tag of "my-inline-selector", like so:', 'soliloquy-lightbox' ),
			'inline_desc_four'			=> __( '<div id="my-inline-selector"><!--content is pulled from here--></div>', 'soliloquy-lightbox' ),
			'keyboard'					=> __( 'Allow Keyboard Shortcuts?', 'soliloquy-lightbox' ),
			'keyboard_desc'				=> __( 'Uncheck if you plan to open forms inside the lightbox.', 'soliloquy-lightbox' ),
			'lightbox_cb'				=> __( 'For more advanced functionality, please click on the contextual help tab at the top of this page.', 'soliloquy-lightbox' ),
			'lightbox_cp_callback'		=> __( 'changepicturecallback', 'soliloquy' ),
			'lightbox_cp_callback_desc'	=> __( ' - Fired when an item is shown or changed. Dynamic action hook for this callback: <code>tgmsp_lightbox_cpcallback_%s</code>', 'soliloquy-lightbox' ),
			'lightbox_callback'			=> __( 'callback', 'soliloquy' ),
			'lightbox_callback_desc'	=> __( ' - Fired when prettyPhoto is closed. Dynamic action hook for this callback: <code>tgmsp_lightbox_callback_%s</code>', 'soliloquy-lightbox' ),
			'lightbox_content'			=> __( 'Lightbox Content Type', 'soliloquy-lightbox' ),
			'lightbox_content_desc'		=> __( 'Choose the type of content you want to display within the lightbox for this image.', 'soliloquy-lightbox' ),
			'lightbox_options'			=> __( 'Lightbox Options', 'soliloquy-lightbox' ),
			'lightbox_settings'			=> __( 'Soliloquy Lightbox Settings', 'soliloquy-lightbox' ),
			'meta_image_desc'			=> __( 'For images and videos from sources like YouTube and Vimeo, simply paste the URL of the image or video in the image link URL field above.', 'soliloquy-lightbox' ),
			'modal'						=> __( 'Use Modal Lightbox?', 'soliloquy-lightbox' ),
			'modal_desc'				=> __( 'Lightbox can only be closed by clicking on the close button.', 'soliloquy-lightbox' ),
			'movie_height'				=> __( 'Movie Height', 'soliloquy-lightbox' ),
			'movie_width'				=> __( 'Movie Width', 'soliloquy-lightbox' ),
			'opacity'					=> __( 'Lightbox Opacity', 'soliloquy-lightbox' ),
			'opacity_desc'				=> __( 'Opacity of the lightbox overlay - must be between 0 and 1.', 'soliloquy-lightbox' ),
			'overlay'					=> __( 'Overlay Gallery?', 'soliloquy-lightbox' ),
			'overlay_desc'				=> __( 'If checked, the gallery will overlay the fullscreen image on mouse over.', 'soliloquy-lightbox' ),
			'padding'					=> __( 'Horizontal Padding', 'soliloquy-lightbox' ),
			'padding_desc'				=> __( 'Amount of padding on each side of the lightbox image area.', 'soliloquy-lightbox' ),
			'resize'					=> __( 'Allow Image Resizing?', 'soliloquy-lightbox' ),
			'resize_desc'				=> __( 'This allows for the resizing of images that are larger than the lightbox area.', 'soliloquy-lightbox' ),
			'select_content_type'		=> __( 'Select the content type for this lightbox: ', 'soliloquy-lightbox' ),
			'separator'					=> __( 'Counter Separator Label', 'soliloquy-lightbox' ),
			'separator_desc'			=> __( 'Separator for the lightbox counter (e.g. 1 / 5).', 'soliloquy-lightbox' ),
			'settings_desc'				=> __( 'You can customize the look and feel of your lightbox setup below. These settings will apply to all lightbox instances within this slider.', 'soliloquy-lightbox' ),
			'slideshow'					=> __( 'Use Gallery as Slideshow?', 'soliloquy-lightbox' ),
			'slideshow_desc'			=> __( 'Select this option to run the lightbox gallery as a slideshow.', 'soliloquy-lightbox' ),
			'social'					=> __( 'Enable Social Buttons?', 'soliloquy-lightbox' ),
			'social_desc'				=> __( 'Check this box to enable social buttons in your lightbox.', 'soliloquy-lightbox' ),
			'speed'						=> __( 'Slideshow Speed', 'soliloquy-lightbox' ),
			'speed_desc'				=> __( 'Set the time between the slideshow transitions.', 'soliloquy-lightbox' ),
			'theme'						=> __( 'Lightbox Theme', 'soliloquy-lightbox' ),
			'title'						=> __( 'Show Image Alt as Title?', 'soliloquy-lightbox' ),
			'title_desc'				=> __( 'Show image alt tag as the image title.', 'soliloquy-lightbox' ),
			'theme_dark_rounded'		=> __( 'Dark Rounded', 'soliloquy-lightbox' ),
			'theme_dark_square'			=> __( 'Dark Square', 'soliloquy-lightbox' ),
			'theme_facebook'			=> __( 'Facebook', 'soliloquy-lightbox' ),
			'theme_light_rounded'		=> __( 'Light Rounded', 'soliloquy-lightbox' ),
			'theme_light_square'		=> __( 'Light Square', 'soliloquy-lightbox' ),
			'theme_pp_default'			=> __( 'Default Theme', 'soliloquy-lightbox' ),
			'wmode'						=> __( 'Wmode Attribute', 'soliloquy-lightbox' ),
			'wmode_desc'				=> __( 'Wmode attribute for Flash (direct, opaque or transparent).', 'soliloquy-lightbox' )
		) );

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
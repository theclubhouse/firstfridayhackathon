<?php
/**
 * Preview class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Preview {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
	
		self::$instance = $this;
		
		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;
		
		add_action( 'tgmsp_preview_start', array( $this, 'preview_init' ) );
		add_action( 'tgmsp_preview_assets', array( $this, 'preview_assets' ) );
		add_action( 'tgmsp_preview_enablers', array( Tgmsp_Lightbox_Shortcode::get_instance(), 'lightbox_init' ) );
	
	}
	
	/**
	 * Init callback to make sure that filters and hooks are only executed in the Preview
	 * context.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_var The $_POST data from the Ajax request
	 */
	public function preview_init( $post_var ) {
	
		/** Only execute if there is a lightbox instance to process */
		foreach ( $post_var as $var ) {
			if ( is_array( $var ) ) {
				if ( ( isset( $var['soliloquy-link'] ) && ! ( empty( $var['soliloquy-link'] ) ) ) && ( isset( $var['soliloquy-lightbox'] ) && 'true' == $var['soliloquy-lightbox'] ) ) {
					add_filter( 'tgmsp_link_output', array( $this, 'apply_lightbox' ), 10, 6 );
					add_filter( 'tgmsp_preview_send_to_script', array( $this, 'lightbox_script' ), 10, 6 );
					break;
				}
			}
		}
	
	}
	
	/**
	 * Loads lightbox preview assets.
	 *
	 * @since 1.0.0
	 */
	public function preview_assets() {
	
		/** Load dev scripts and styles if in Soliloquy dev mode */
		$dev = defined( 'SOLILOQUY_DEV' ) && SOLILOQUY_DEV ? '-dev' : '';
	
		wp_register_script( 'soliloquy-lightbox-preview', plugins_url( 'js/preview' . $dev . '.js', dirname( dirname( __FILE__ ) ) ), array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'soliloquy-lightbox-preview' );
	
	}
	
	/**
	 * Outputs necessary HTML for the lightbox to function.
	 *
	 * @since 1.0.0
	 *
	 * @param string $link The current HTML string for the image link
	 * @param int $id The current slider ID
	 * @param array $image The current image metadata
	 * @param string $title The current image link title
	 * @param string $target The current image link target
	 * @param array $post_var The $_POST data from the Ajax request
	 * @return string $link Amended link with lightbox support
	 */
	public function apply_lightbox( $link, $id, $image, $title, $target, $post_var ) {
		
		/** Instead of getting the post meta, let's set it to the post data instead */
		$pp_settings = $post_var;
		
		/** Only alter the link structure if the lightbox setting is checked */
		if ( isset( $image['lightbox'] ) && $image['lightbox'] ) {
			if ( isset( $image['lightbox_content'] ) ) {
				switch ( $image['lightbox_content'] ) {
					case 'flash' :
						$image['link'] = add_query_arg( array( 'width' => esc_attr( $image['lightbox_flash_width'] ), 'height' => esc_attr( $image['lightbox_flash_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['soliloquy-lightbox-gallery'] ) && ( 'true' == $pp_settings['soliloquy-lightbox-gallery'] ) )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[flash]" title="' . $title . '" ' . $target . '>';
						break;
					case 'quicktime' : 
						$image['link'] = add_query_arg( array( 'width' => esc_attr( $image['lightbox_qt_width'] ), 'height' => esc_attr( $image['lightbox_qt_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['soliloquy-lightbox-gallery'] ) && ( 'true' == $pp_settings['soliloquy-lightbox-gallery'] ) )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[movies]" title="' . $title . '" ' . $target . '>';
						break;
					case 'iframe' :
						$image['link'] = add_query_arg( array( 'iframe' => 'true', 'width' => esc_attr( $image['lightbox_iframe_width'] ), 'height' => esc_attr( $image['lightbox_iframe_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['soliloquy-lightbox-gallery'] ) && ( 'true' == $pp_settings['soliloquy-lightbox-gallery'] ) )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[iframes]" title="' . $title . '" ' . $target . '>';
						break;
					case 'ajax' :
						$image['link'] = add_query_arg( array( 'ajax' => 'true', 'width' => esc_attr( $image['lightbox_ajax_width'] ), 'height' => esc_attr( $image['lightbox_ajax_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['soliloquy-lightbox-gallery'] ) && ( 'true' == $pp_settings['soliloquy-lightbox-gallery'] ) )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[ajax]" title="' . $title . '" ' . $target . '>';
						break;
					default :
						if ( isset( $pp_settings['soliloquy-lightbox-gallery'] ) && ( 'true' == $pp_settings['soliloquy-lightbox-gallery'] ) )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto" title="' . $title . '" ' . $target . '>';
						break;
				}
			}
			
			$link = apply_filters( 'tgmsp_lightbox_link_filtered', $link, $id, $image, $title, $target, $pp_settings );
		}
				
		return $link;
				
	}
	
	/**
	 * Filters the 'extra' array key to pass lightbox init information to the Ajax script.
	 *
	 * @since 1.0.0
	 *
	 * @param array $data Data that is to be sent to the Ajax success function
	 * @param int $id The current slider ID
	 * @param array $images Array of images for the slider
	 * @param array $soliloquy_data Array of settings data for the slider
	 * @param int $soliloquy_count The current slider number on the page
	 * @param array $post_var The $_POST settings sent from the Ajax script
	 * @return array $data Amended variable with lightbox support
	 */
	public function lightbox_script( $data, $id, $images, $soliloquy_data, $soliloquy_count, $post_var ) {
	
		/** Add our lightbox script to be processed */
		$data['scripts'][] = plugins_url( 'js/jquery.prettyPhoto.js', dirname( dirname( __FILE__ ) ) );
		
		/** Get all the data prepared to output the correct settings for the lightbox script */
		$theme		= isset( $post_var['soliloquy-lightbox-theme'] ) ? $post_var['soliloquy-lightbox-theme'] : 'pp_default';
		$anispeed	= isset( $post_var['soliloquy-lightbox-animation'] ) ? $post_var['soliloquy-lightbox-animation'] : 'fast';
		$opacity	= isset( $post_var['soliloquy-lightbox-opacity'] ) ? $post_var['soliloquy-lightbox-opacity'] : .8;
		$slideshow 	= ( isset( $post_var['soliloquy-lightbox-gallery'] ) && ( 'true' == $post_var['soliloquy-lightbox-gallery'] ) ) && ( isset( $post_var['soliloquy-lightbox-slideshow'] ) && ( 'true' == $post_var['soliloquy-lightbox-slideshow'] ) ) ? 'true' : 'false';
		$speed 		= ( 'true' == $slideshow ) ? 'slideshow: ' . $post_var['soliloquy-lightbox-slideshow-speed'] . ',' : 'slideshow: false,';
		$title 		= isset( $post_var['soliloquy-lightbox-title'] ) && ( 'true' == $post_var['soliloquy-lightbox-title'] ) ? 'true' : 'false';
		$resize 	= isset( $post_var['soliloquy-lightbox-resize'] ) && ( 'true' == $post_var['soliloquy-lightbox-resize'] ) ? 'true' : 'false';
		$width 		= isset( $post_var['soliloquy-lightbox-width'] ) && ( 'true' == $post_var['soliloquy-lightbox-width'] ) ? absint( $post_var['soliloquy-lightbox-width'] ) : 500;
		$height 	= isset( $post_var['soliloquy-lightbox-height'] ) && ( 'true' == $post_var['soliloquy-lightbox-height'] ) ? absint( $post_var['soliloquy-lightbox-height'] ) : 344;
		$sep		= isset( $post_var['soliloquy-lightbox-separator'] ) ? esc_attr( $post_var['soliloquy-lightbox-separator'] ) : '/';
		$padding	= isset( $post_var['soliloquy-lightbox-padding'] ) ? absint( $post_var['soliloquy-lightbox-padding'] ) : 20;
		$flash 		= isset( $post_var['soliloquy-lightbox-flash'] ) && ( 'true' == $post_var['soliloquy-lightbox-flash'] ) ? 'true' : 'false';
		$wmode		= isset( $post_var['soliloquy-lightbox-wmode'] ) ? strip_tags( esc_attr( $post_var['soliloquy-lightbox-wmode'] ) ) : 'opaque';
		$autoplay 	= isset( $post_var['soliloquy-lightbox-autoplay'] ) && ( 'true' == $post_var['soliloquy-lightbox-autoplay'] ) ? 'true' : 'false';
		$modal 		= isset( $post_var['soliloquy-lightbox-modal'] ) && ( 'true' == $post_var['soliloquy-lightbox-modal'] ) ? 'true' : 'false';
		$deep 		= isset( $post_var['soliloquy-lightbox-deeplinking'] ) && ( 'true' == $post_var['soliloquy-lightbox-deeplinking'] ) ? 'true' : 'false';
		$overlay 	= isset( $post_var['soliloquy-lightbox-overlay'] ) && ( 'true' == $post_var['soliloquy-lightbox-overlay'] ) ? 'true' : 'false';
		$keyboard 	= isset( $post_var['soliloquy-lightbox-keyboard'] ) && ( 'true' == $post_var['soliloquy-lightbox-keyboard'] ) ? 'true' : 'false';
		$ie6 		= isset( $post_var['soliloquy-lightbox-ie6'] ) && ( 'true' == $post_var['soliloquy-lightbox-ie6'] ) ? 'true' : 'false';
		$social 	= isset( $post_var['soliloquy-lightbox-social'] ) && ( 'true' == $post_var['soliloquy-lightbox-social'] ) ? '' : 'social_tools: false,';
	
		/** Darn you, output buffering! */
		ob_start();
		do_action( 'tgmsp_lightbox_cpcallback_' . absint( $id ) );
		$lb_cp = ob_get_clean();
		
		ob_start();
		do_action( 'tgmsp_lightbox_callback_' . absint( $id ) );
		$lb_cb = ob_get_clean();
		
		ob_start();
		do_action( 'tgmsp_lightbox_script_' . absint( $id ) );
		$lb_sc = ob_get_clean();
	
		/** Append our script information */
		$data['extra'] .= '<link rel="stylesheet" href="' . plugins_url( 'css/prettyPhoto.css', dirname( dirname( __FILE__ ) ) ) . '" type="text/css" media="screen" />'; 
		$data['extra'] .= '<script type="text/javascript">';
		$data['extra'] .= 'jQuery("#flexslider-' . absint( $id ) . ' .clone a").removeAttr("rel");';
		$data['extra'] .= 'jQuery("#flexslider-' . absint( $id ) . ' a[rel^=\'prettyPhoto\']").prettyPhoto({';
		$data['extra'] .= 'theme: \'' . $theme . '\',';
		$data['extra'] .= 'animation_speed: \'' . $anispeed . '\',';
		$data['extra'] .= 'opacity: \'' . $opacity . '\',';
		$data['extra'] .= $social;
		$data['extra'] .= 'autoplay_slideshow: ' . $slideshow . ',';
		$data['extra'] .= $speed;
		$data['extra'] .= 'show_title: \'' . $title . '\',';
		$data['extra'] .= 'allow_resize: \'' . $resize . '\',';
		$data['extra'] .= 'default_width: \'' . $width . '\',';
		$data['extra'] .= 'default_height: \'' . $height . '\',';
		$data['extra'] .= 'counter_separator_label: \'' . $sep . '\',';
		$data['extra'] .= 'horizontal_padding: \'' . $padding . '\',';
		$data['extra'] .= 'hideflash: \'' . $flash . '\',';
		$data['extra'] .= 'wmode: \'' . $wmode . '\',';
		$data['extra'] .= 'autoplay: \'' . $autoplay . '\',';
		$data['extra'] .= 'modal: \'' . $modal . '\',';
		$data['extra'] .= 'deeplinking: \'' . $deep . '\',';
		$data['extra'] .= 'overlay_gallery: \'' . $overlay . '\',';
		$data['extra'] .= 'keyboard_shortcuts: \'' . $keyboard . '\',';
		$data['extra'] .= 'ie6_fallback: \'' . $ie6 . '\',';
		$data['extra'] .= 'changepicturecallback: function() {soliloquySlider' . absint( $id ) . '.pause(); ' . $lb_cp . '},';
		$data['extra'] .= 'callback: function() {soliloquySlider' . absint( $id ) . '.resume(); ' . $lb_cb . '},';
		$data['extra'] .= $lb_sc;
		$data['extra'] .= '});';
		$data['extra'] .= '</script>';
		
		return $data;
	
	}
	
	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {
	
		return self::$instance;
	
	}
	
}
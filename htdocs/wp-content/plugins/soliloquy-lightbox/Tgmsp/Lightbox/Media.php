<?php
/**
 * Media class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Media {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;

		add_action( 'load-media-upload.php', array( $this, 'load_media_upload' ) );
		add_filter( 'attachment_fields_to_edit', array( $this, 'media' ), 20, 2 );
		add_filter( 'attachment_fields_to_save', array( $this, 'update_media' ), 20, 2 );

	}

	/**
	 * Runs on the load-media-upload.php action hook and conditionally
	 * enqueues a script for better handling of lightbox elements within
	 * the media uploader.
	 *
	 * @since 1.0.0
	 */
	public function load_media_upload() {

		if ( Tgmsp_Media::get_instance()->is_our_context() )
			add_action( 'admin_head', array( $this, 'admin_head' ) );

	}

	/**
	 * Removes any unnecessary elements during the slider insert and
	 * update process via CSS.
	 *
	 * @since 1.0.0
	 */
	public function admin_head() {

		?>
		<style type="text/css">#media-items .soliloquy_lightbox_content, #media-items .soliloquy_lightbox_flash_width, #media-items .soliloquy_lightbox_flash_height, #media-items .soliloquy_lightbox_qt_width, #media-items .soliloquy_lightbox_qt_height, #media-items .soliloquy_lightbox_iframe_width, #media-items .soliloquy_lightbox_iframe_height, #media-items .soliloquy_lightbox_ajax_width, #media-items .soliloquy_lightbox_ajax_height { display: none !important; }</style>
		<?php

	}

	/**
	 * Adds checkbox for enabling lightbox support in the attachment
	 * upload form along with other lightbox specific elements.
	 *
	 * @since 1.0.0
	 *
	 * @global object $current_screen The current screen object
	 * @param array $fields Default list of attachment fields
	 * @return array $fields Amended list of attachment fields
	 */
	public function media( $fields, $attachment ) {

		global $current_screen;

		if ( Tgmsp_Media::get_instance()->is_our_context() || 'soliloquy' == $current_screen->post_type ) {
			/** Checkbox for enabling lightbox */
			$fields['soliloquy_lightbox'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['enable_lightbox'],
				'input' => 'html',
				'html' 	=> '<input id="attachments[' . $attachment->ID . '][soliloquy_lightbox]" name="attachments[' . $attachment->ID . '][soliloquy_lightbox]" type="checkbox" value="' . get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ) . '"' . checked( get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ), 1, false ) . ' />'
			);

			/** Build select output for determining type of content to serve in the lightbox */
			$types = Tgmsp_Lightbox_Ajax::get_instance()->types();
			$output = '<select id="attachments[' . $attachment->ID . '][soliloquy_lightbox_content]" name="attachments[' . $attachment->ID . '][soliloquy_lightbox_content]">';
			$output .= '<option value="image">' . Tgmsp_Lightbox_Strings::get_instance()->strings['image_video_link'] . '</option>';
			foreach ( $types as $type )
				$output .= '<option value="' . esc_attr( $type ) . '"' . selected( $type, get_post_meta( $attachment->ID, '_soliloquy_lightbox_content', true ), false ) . '>' . esc_html( $type ) . '</option>';
			$output .= '</select>';
			$fields['soliloquy_lightbox_content'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_content'],
				'input' => 'html',
				'html'	=> $output,
				'helps'	=> Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_content_desc']
			);

			/** Flash width/height fields */
			$fields['soliloquy_lightbox_flash_width'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['flash_width'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_width', true )
			);
			$fields['soliloquy_lightbox_flash_height'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['flash_height'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_width', true )
			);

			/** QuickTime movie width/height fields */
			$fields['soliloquy_lightbox_qt_width'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['movie_width'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_width', true )
			);
			$fields['soliloquy_lightbox_qt_height'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['movie_height'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_width', true )
			);

			/** Iframe width/height fields */
			$fields['soliloquy_lightbox_iframe_width'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_width'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_width', true )
			);
			$fields['soliloquy_lightbox_iframe_height'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_height'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_width', true )
			);

			/**Ajax width/height fields */
			$fields['soliloquy_lightbox_ajax_width'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_width'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_width', true )
			);
			$fields['soliloquy_lightbox_ajax_height'] = array(
				'label' => Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_height'],
				'input' => 'text',
				'value' => get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_width', true )
			);

			$fields = apply_filters( 'tgmsp_lightbox_attachment_fields', $fields, $attachment );
		}

		return $fields;

	}

	/**
	 * Saves checkbox state when inserting or saving the newly uploaded
	 * attachment file.
	 *
	 * @since 1.0.0
	 *
	 * @global object $current_screen The current screen object
	 * @param object $attachment The current attachment object
	 * @param array $post_var $_POST array keys sent when updating attachment meta
	 * @return object $attachment Return the attachment after updating post meta
	 */
	public function update_media( $attachment, $post_var ) {

		global $current_screen;

		if ( Tgmsp_Media::get_instance()->is_our_context() || 'soliloquy' == $current_screen->post_type ) {
			update_post_meta( $attachment['ID'], '_soliloquy_image_lightbox', isset( $post_var['soliloquy_lightbox'] ) ? 1 : 0 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_content', preg_replace( '#[^a-z0-9-_]#', '', $post_var['soliloquy_lightbox_content'] ) );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_flash_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_flash_width'] ) ) ? trim( $post_var['soliloquy_lightbox_flash_width'] ) : 500 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_flash_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_flash_height'] ) ) ? trim( $post_var['soliloquy_lightbox_flash_height'] ) : 344 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_qt_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_qt_width'] ) ) ? trim( $post_var['soliloquy_lightbox_qt_width'] ) : 500 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_qt_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_qt_height'] ) ) ? trim( $post_var['soliloquy_lightbox_qt_height'] ) : 344 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_iframe_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_iframe_width'] ) ) ? trim( $post_var['soliloquy_lightbox_iframe_width'] ) : 500 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_iframe_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_iframe_height'] ) ) ? trim( $post_var['soliloquy_lightbox_iframe_height'] ) : 344 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_ajax_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_ajax_width'] ) ) ? trim( $post_var['soliloquy_lightbox_ajax_width'] ) : 500 );
			update_post_meta( $attachment['ID'], '_soliloquy_lightbox_ajax_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy_lightbox_ajax_height'] ) ) ? trim( $post_var['soliloquy_lightbox_ajax_height'] ) : 344 );

			do_action( 'tgmsp_lightbox_attachment_fields_update', $attachment, $post_var );
		}

		return $attachment;

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
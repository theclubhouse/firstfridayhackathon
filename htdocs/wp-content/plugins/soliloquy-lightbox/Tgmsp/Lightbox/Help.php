<?php
/**
 * Help class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Help {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
	
		self::$instance = $this;
		
		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;
	
		add_action( 'admin_head', array( $this, 'contextual_help' ) );
	
	}
	
	/**
	 * Adds contextual help to Soliloquy pages.
	 *
	 * @since 1.0.0
	 *
	 * @global object $current_screen The current screen object
	 * @global object $post The current post object
	 */
	public function contextual_help() {
	
		global $current_screen, $post;
		
		/** Set help for the Add New and Edit screens */
		if ( 'post' == $current_screen->base && 'soliloquy' == $current_screen->post_type ) {
			$current_screen->add_help_tab( array(
				'id'		=> 'soliloquy-lightbox-advanced-help',
				'title'		=> Tgmsp_Lightbox_Strings::get_instance()->strings['advanced_help'],
				'content'	=> sprintf( '<p><strong>%s</strong></p><p><code>%s</code><span>%s</span><br /><code>%s</code><span>%s</span><p>', Tgmsp_Lightbox_Strings::get_instance()->strings['advanced_help_desc'], Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_cp_callback'], sprintf( Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_cp_callback_desc'], $post->ID ), Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_callback'], sprintf( Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_callback_desc'], $post->ID ) )
			) );
		}
	
	}
	
	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {
	
		return self::$instance;
	
	}
	
}
<?php
/**
 * Shortcode class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Shortcode {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Holds array of slider IDs that need lightbox initiation.
	 *
	 * @since 1.1.2
	 *
	 * @var object
	 */
	public $lightboxes = array();

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;

		/** Customize the shortcode output for lightbox */
		add_filter( 'tgmsp_image_data', array( $this, 'lightbox_data' ), 10, 2 );
		add_action( 'tgmsp_before_slider_output', array( $this, 'shortcode_script' ), 10, 4 );
		add_filter( 'tgmsp_link_output', array( $this, 'apply_lightbox' ), 10, 5 );

	}

	/**
	 * Send lightbox data when Soliloquy grabs image meta.
	 *
	 * @since 1.0.0
	 *
	 * @param array $image Image data Soliloquy uses to send to the current slider
	 * @param object $attachment The current attachment object
	 * @return array $image Amended image data with lightbox meta
	 */
	public function lightbox_data( $image, $attachment ) {

		$image['lightbox'] 					= get_post_meta( $image['id'], '_soliloquy_image_lightbox', true );
		$image['lightbox_content'] 			= get_post_meta( $image['id'], '_soliloquy_lightbox_content', true );
		$image['lightbox_flash_width'] 		= get_post_meta( $image['id'], '_soliloquy_lightbox_flash_width', true );
		$image['lightbox_flash_height'] 	= get_post_meta( $image['id'], '_soliloquy_lightbox_flash_height', true );
		$image['lightbox_qt_width'] 		= get_post_meta( $image['id'], '_soliloquy_lightbox_qt_width', true );
		$image['lightbox_qt_height'] 		= get_post_meta( $image['id'], '_soliloquy_lightbox_qt_height', true );
		$image['lightbox_iframe_width'] 	= get_post_meta( $image['id'], '_soliloquy_lightbox_iframe_width', true );
		$image['lightbox_iframe_height'] 	= get_post_meta( $image['id'], '_soliloquy_lightbox_iframe_height', true );
		$image['lightbox_ajax_width'] 		= get_post_meta( $image['id'], '_soliloquy_lightbox_ajax_width', true );
		$image['lightbox_ajax_height'] 		= get_post_meta( $image['id'], '_soliloquy_lightbox_ajax_height', true );

		// Possibly autolink images in the lightbox.
		$lightbox_meta =  get_post_meta( $attachment->post_parent, '_soliloquy_lightbox', true );
		if ( isset( $lightbox_meta['auto'] ) && $lightbox_meta['auto'] ) {
    		$link 				= wp_get_attachment_image_src( $attachment->ID, 'full' );
            $image['lightbox'] 	= true;
            $image['link'] 		= $link[0];
		}

		return apply_filters( 'tgmsp_lightbox_data', $image, $attachment );

	}

	/**
	 * Enqueue prettyPhoto script and style if an image is found that has enabled lightbox.
	 *
	 * @since 1.0.0
	 *
	 * @param array $images All of the images within the slider
	 * @param array $soliloquy_data Slider metadata
	 * @param int $soliloquy_count The current iteration of the slider instance
	 * @return null Return early if there are no images to loop through
	 */
	public function shortcode_script( $id, $images, $soliloquy_data, $soliloquy_count ) {

		/** Return early if there are no images to loop through */
		if ( empty( $images ) )
			return;

		/** Loop through images, and if one has lightbox enabled, store it's ID */
		foreach ( $images as $image ) {
			if ( isset( $image['lightbox'] ) && $image['lightbox'] && ! empty( $image['link'] ) ) {
				$this->lightboxes[] = absint( $id );
				break;
			}
		}

		/** Loop through images, and if one has lightbox enabled, load lightbox js/css and break */
		foreach ( $images as $image ) {
			if ( isset( $image['lightbox'] ) && $image['lightbox'] && ! empty( $image['link'] ) ) {
				wp_enqueue_script( 'soliloquy-lightbox' );
				wp_enqueue_style( 'soliloquy-lightbox' );
				add_action( 'wp_footer', array( $this, 'lightbox_init' ), 99 );
				do_action( 'tgmsp_lightbox_is_active', $id, $images, $soliloquy_data, $soliloquy_count );
				break;
			}
		}

	}

	/**
	 * Outputs necessary HTML for the lightbox to function.
	 *
	 * @since 1.0.0
	 *
	 * @param string $link The current HTML string for the image link
	 * @param int $id The current slider ID
	 * @param array $image The current image metadata
	 * @param string $title The current image link title
	 * @param string $target The current image link target
	 * @return string $link Amended link with lightbox support
	 */
	public function apply_lightbox( $link, $id, $image, $title, $target ) {

		$pp_settings = get_post_meta( $id, '_soliloquy_lightbox', true );

		/** Only alter the link structure if the lightbox setting is checked */
		if ( isset( $image['lightbox'] ) && $image['lightbox'] ) {
			if ( isset( $image['lightbox_content'] ) ) {
				switch ( $image['lightbox_content'] ) {
					case 'flash' :
						$image['link'] = add_query_arg( array( 'width' => esc_attr( $image['lightbox_flash_width'] ), 'height' => esc_attr( $image['lightbox_flash_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[flash]" title="' . $title . '" ' . $target . '>';
						break;
					case 'quicktime' :
						$image['link'] = add_query_arg( array( 'width' => esc_attr( $image['lightbox_qt_width'] ), 'height' => esc_attr( $image['lightbox_qt_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[movies]" title="' . $title . '" ' . $target . '>';
						break;
					case 'iframe' :
						$image['link'] = add_query_arg( array( 'iframe' => 'true', 'width' => esc_attr( $image['lightbox_iframe_width'] ), 'height' => esc_attr( $image['lightbox_iframe_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[iframes]" title="' . $title . '" ' . $target . '>';
						break;
					case 'ajax' :
						$image['link'] = add_query_arg( array( 'ajax' => 'true', 'width' => esc_attr( $image['lightbox_ajax_width'] ), 'height' => esc_attr( $image['lightbox_ajax_height'] ) ), $image['link'] );
						if ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[ajax]" title="' . $title . '" ' . $target . '>';
						break;
					default :
						if ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] )
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto[pp_gal]" title="' . $title . '" ' . $target . '>';
						else
							$link = '<a href="' . esc_url( $image['link'] ) . '" rel="prettyPhoto" title="' . $title . '" ' . $target . '>';
						break;
				}
			}

			$link = apply_filters( 'tgmsp_lightbox_link_filtered', $link, $id, $image, $title, $target, $pp_settings );
		}

		// Load fitvids to ensure responsive styling of any video objects.
		wp_enqueue_script( 'soliloquy-fitvids' );

		return $link;

	}

	/**
	 * Lightbox callback for initializing the lightbox.
	 *
	 * @since 1.0.0
	 *
	 * @global array $soliloquy_data Metadata for the current slider
	 */
	public function lightbox_init() {

		global $soliloquy_data;

		foreach ( $soliloquy_data as $i => $slider ) {
			/** Pass over any sliders that do not need lightbox initialization */
			if ( ! in_array( absint( $slider['id'] ), (array) $this->lightboxes ) )
				continue;

			/** Setup variables for output */
			$pp_settings = get_post_meta( $slider['id'], '_soliloquy_lightbox', true );

			$theme		= isset( $pp_settings['lightbox_theme'] ) 	? $pp_settings['lightbox_theme'] : 'pp_default';
			$anispeed	= isset( $pp_settings['animation'] ) 		? $pp_settings['animation'] : 'fast';
			$opacity	= isset( $pp_settings['opacity'] ) 			? $pp_settings['opacity'] : .8;
			$slideshow 	= ( isset( $pp_settings['lightbox_gallery'] ) && $pp_settings['lightbox_gallery'] ) && ( isset( $pp_settings['lightbox_slideshow'] ) && $pp_settings['lightbox_slideshow'] ) ? 'true' : 'false';
			$speed 		= ( 'true' == $slideshow ) ? 'slideshow:' . $pp_settings['lightbox_slideshow_speed'] . ',' : 'slideshow:false,';
			$title 		= isset( $pp_settings['title'] ) 			&& $pp_settings['title'] 		? 'true' : 'false';
			$resize 	= isset( $pp_settings['resize'] ) 			&& $pp_settings['resize'] 		? 'true' : 'false';
			$width 		= isset( $pp_settings['width'] ) 			&& $pp_settings['width'] 		? $pp_settings['width'] : 500;
			$height 	= isset( $pp_settings['height'] ) 			&& $pp_settings['height'] 		? $pp_settings['height'] : 344;
			$sep		= isset( $pp_settings['separator'] ) 		? $pp_settings['separator'] : '/';
			$padding	= isset( $pp_settings['padding'] ) 			? $pp_settings['padding'] : 20;
			$flash 		= isset( $pp_settings['flash'] ) 			&& $pp_settings['flash'] 		? 'true' : 'false';
			$wmode		= isset( $pp_settings['wmode'] ) 			? $pp_settings['wmode'] : 'opaque';
			$autoplay 	= isset( $pp_settings['autoplay'] ) 		&& $pp_settings['autoplay'] 	? 'true' : 'false';
			$modal 		= isset( $pp_settings['modal'] ) 			&& $pp_settings['modal'] 		? 'true' : 'false';
			$deep 		= isset( $pp_settings['deeplinking'] ) 		&& $pp_settings['deeplinking'] 	? 'true' : 'false';
			$overlay 	= isset( $pp_settings['overlay'] ) 			&& $pp_settings['overlay'] 		? 'true' : 'false';
			$keyboard 	= isset( $pp_settings['keyboard'] ) 		&& $pp_settings['keyboard'] 	? 'true' : 'false';
			$ie6 		= isset( $pp_settings['ie6'] ) 				&& $pp_settings['ie6'] 			? 'true' : 'false';
			$social 	= isset( $pp_settings['social'] ) 			&& $pp_settings['social'] 		? '' : 'social_tools:false,';

			/** Do a check for Soliloquy versin to ensure that correct API call is sent */
			$api = version_compare( Tgmsp::get_instance()->version, '1.3.0', '<' ) ? '.resume();' : '.play();';
			?>
			<script type="text/javascript">jQuery(document).ready(function($){$('#soliloquy-<?php echo absint( $slider['id'] ); ?> .clone a').removeAttr('rel');$('#soliloquy-<?php echo absint( $slider['id'] ); ?> a[rel^="prettyPhoto"]').prettyPhoto({theme:'<?php echo $theme; ?>',animation_speed:'<?php echo $anispeed; ?>',opacity:<?php echo $opacity; ?>,<?php echo $social; ?>autoplay_slideshow:<?php echo $slideshow; ?>,<?php echo $speed; ?>show_title:<?php echo $title; ?>,allow_resize:<?php echo $resize; ?>,default_width:<?php echo absint( $width ); ?>,default_height:<?php echo absint( $height ); ?>,counter_separator_label:'<?php echo $sep; ?>',horizontal_padding:<?php echo absint( $padding ); ?>,hideflash:<?php echo $flash; ?>,wmode:'<?php echo $wmode; ?>',autoplay:<?php echo $autoplay; ?>,modal:<?php echo $modal; ?>,deeplinking:<?php echo $deep; ?>,overlay_gallery:<?php echo $overlay; ?>,keyboard_shortcuts:<?php echo $keyboard; ?>,ie6_fallback:<?php echo $ie6; ?>,<?php do_action( 'tgmsp_lightbox_script_' . absint( $slider['id'] ) ); ?>changepicturecallback:function() {soliloquySlider<?php echo absint( $slider['id'] ); ?>.pause();<?php do_action( 'tgmsp_lightbox_cpcallback_' . absint( $slider['id'] ) ); ?>},callback:function(){soliloquySlider<?php echo absint( $slider['id'] ) . $api; do_action( 'tgmsp_lightbox_callback_' . absint( $slider['id'] ) ); ?>}});});</script>
			<?php
		}

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
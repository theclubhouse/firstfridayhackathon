<?php
/**
 * Ajax class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Ajax {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
	
		self::$instance = $this;
		
		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;
	
		add_filter( 'tgmsp_ajax_refresh_callback', array( $this, 'refresh_images' ), 20, 2 );
		add_action( 'tgmsp_after_meta_defaults', array( $this, 'lightbox' ) );
		add_action( 'tgmsp_ajax_update_meta', array( $this, 'save_meta' ) );
	
	}
	
	/**
	 * Adds lightbox fields to images array when refreshing images.
	 *
	 * @since 1.0.0
	 *
	 * @param array $data Data sent back to jQuery script for outputting
	 * @param object $attachment The current attachment object
	 * @return array $data Amended array of data with our lightbox info
	 */
	public function refresh_images( $data, $attachment ) {
		
		$types 		= $this->types();
		$content 	= '';
		
		$content 	.= '<label for="soliloquy-lightbox-content-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['select_content_type'] . '</label>';
		$content 	.= '<select id="soliloquy-lightbox-content-' . $attachment->ID . '" class="soliloquy-lightbox-content" name="_soliloquy_lightbox[content]">';
		$content 		.= '<option value="image">' . Tgmsp_Lightbox_Strings::get_instance()->strings['image_video_link'] . '</option>';
		foreach ( (array) $types as $type )
		$content 		.= '<option value="' . esc_attr( $type ) . '"' . selected( $type, get_post_meta( $attachment->ID, '_soliloquy_lightbox_content', true ), false ) . '>' . esc_html( $type ) . '</option>';
		$content 	.= '</select><br /><br />';
		$content 	.= '<div id="soliloquy-lightbox-image-desc" class="lightbox-desc"><p>' . Tgmsp_Lightbox_Strings::get_instance()->strings['meta_image_desc'] . '</p></div>';
		$content 	.= '<div id="soliloquy-lightbox-flash-desc" class="lightbox-desc">';
		$content 		.= '<p><label for="soliloquy-lightbox-flash-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['flash_width'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-flash-width-' . $attachment->ID . '" class="soliloquy-lightbox-flash-width" type="text" name="_soliloquy_lightbox[flash_width]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_width', true ) ) . '" /><br />';
		$content 		.= '<label for="soliloquy-lightbox-flash-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['flash_height'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-flash-height-' . $attachment->ID . '" class="soliloquy-lightbox-flash-height" type="text" name="_soliloquy_lightbox[flash_height]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_height', true ) ) . '" /></p>';
		$content 		.= '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
		$content 	.= '</div>';
		$content 	.= '<div id="soliloquy-lightbox-qt-desc" class="lightbox-desc">';
		$content 		.= '<p><label for="soliloquy-lightbox-qt-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['movie_width'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-qt-width-' . $attachment->ID . '" class="soliloquy-lightbox-qt-width" type="text" name="_soliloquy_lightbox[qt_width]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_width', true ) ) . '" /><br />';
		$content 		.= '<label for="soliloquy-lightbox-qt-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['movie_height'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-qt-height-' . $attachment->ID . '" class="soliloquy-lightbox-qt-height" type="text" name="_soliloquy_lightbox[qt_height]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_height', true ) ) . '" /></p>';
		$content 		.= '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
		$content 	.= '</div>';
		$content 	.= '<div id="soliloquy-lightbox-iframe-desc" class="lightbox-desc">';
		$content 		.= '<p><label for="soliloquy-lightbox-iframe-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_width'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-iframe-width-' . $attachment->ID . '" class="soliloquy-lightbox-iframe-width" type="text" name="_soliloquy_lightbox[iframe_width]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_width', true ) ) . '" /><br />';
		$content 		.= '<label for="soliloquy-lightbox-iframe-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_height'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-iframe-height-' . $attachment->ID . '" class="soliloquy-lightbox-iframe-height" type="text" name="_soliloquy_lightbox[iframe_height]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_height', true ) ) . '" /></p>';
		$content 		.= '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
		$content 	.= '</div>';
		$content 	.= '<div id="soliloquy-lightbox-inline-desc" class="lightbox-desc">';
		$content 		.= sprintf( '<p>%s</p><p><strong>%s</strong></p><p>%s</p><p><strong>%s</strong></p>', Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_one'], Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_two'], Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_three'], htmlentities( Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_four'] ) );
		$content 	.= '</div>';
		$content 	.= '<div id="soliloquy-lightbox-ajax-desc" class="lightbox-desc">';
		$content 		.= '<p><label for="soliloquy-lightbox-ajax-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_width'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-ajax-width-' . $attachment->ID . '" class="soliloquy-lightbox-ajax-width" type="text" name="_soliloquy_lightbox[ajax_width]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_width', true ) ) . '" /><br />';
		$content 		.= '<label for="soliloquy-lightbox-ajax-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_height'] . '</label>';
		$content 		.= '<input id="soliloquy-lightbox-ajax-height-' . $attachment->ID . '" class="soliloquy-lightbox-ajax-height" type="text" name="_soliloquy_lightbox[ajax_height]" value="' . absint( get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_height', true ) ) . '" /></p>';
		$content 		.= '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
		$content 	.= '</div>';
		$content	= apply_filters( 'tgmsp_lightbox_ajax_content', $content, $data, $attachment );
		
		/** Send content within data array */
		$data['after_meta_defaults']['lightbox'] = '<tr id="soliloquy-lightbox-box-' . $attachment->ID . '" valign="middle">' . '<th scope="row"><label for="soliloquy-lightbox-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['enable_lightbox'] . '</label></th>' . '<td>' . '<input id="soliloquy-lightbox-' . $attachment->ID . '" class="soliloquy-lightbox" type="checkbox" name="_soliloquy_lightbox[lightbox]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ) ) . '"' . checked( get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ), 1, false ) . ' /> ' . '<span class="description">' . Tgmsp_Lightbox_Strings::get_instance()->strings['enable_lightbox_desc'] . '</span></td>' . '</tr>' . '<tr id="soliloquy-lightbox-extras-' . $attachment->ID . '" valign="top">' . '<th scope="row">' . Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_options'] . '</th>' . '<td id="soliloquy-lightbox-options-' . $attachment->ID . '" class="soliloquy-lightbox-options">' . $content . '</td>' . '</tr>';
			
		return apply_filters( 'tgmsp_lightbox_ajax_data', $data, $attachment );
		
	}
	
	/**
	 * Outputs the necessary HTML for enabling lightbox for the image.
	 *
	 * @since 1.0.0
	 *
	 * @param object $attachment The current attachment object
	 */
	public function lightbox( $attachment ) {
		
		echo '<tr id="soliloquy-lightbox-box-' . $attachment->ID . '" valign="middle">';
			echo '<th scope="row"><label for="soliloquy-lightbox-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['enable_lightbox'] . '</label></th>';
			echo '<td>';
				echo '<input id="soliloquy-lightbox-' . $attachment->ID . '" class="soliloquy-lightbox" type="checkbox" name="_soliloquy_lightbox[lightbox]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ) ) . '"' . checked( get_post_meta( $attachment->ID, '_soliloquy_image_lightbox', true ), 1, false ) . ' /> ';
				echo '<span class="description">' . Tgmsp_Lightbox_Strings::get_instance()->strings['enable_lightbox_desc'] . '</span>';
			echo '</td>';
		echo '</tr>';
			
		echo '<tr id="soliloquy-lightbox-extras-' . $attachment->ID . '" valign="top">';
			echo '<th scope="row">' . Tgmsp_Lightbox_Strings::get_instance()->strings['lightbox_options'] . '</th>';
			echo '<td id="soliloquy-lightbox-options-' . $attachment->ID . '" class="soliloquy-lightbox-options">';
				$types = $this->types();
				echo '<label for="soliloquy-lightbox-content-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['select_content_type'] . '</label>';
				echo '<select id="soliloquy-lightbox-content-' . $attachment->ID . '" class="soliloquy-lightbox-content" name="_soliloquy_lightbox[content]">';
				echo '<option value="image">' . Tgmsp_Lightbox_Strings::get_instance()->strings['image_video_link'] . '</option>';
				foreach ( (array) $types as $type )
					echo '<option value="' . esc_attr( $type ) . '"' . selected( $type, get_post_meta( $attachment->ID, '_soliloquy_lightbox_content', true ), false ) . '>' . esc_html( $type ) . '</option>';
				echo '</select><br /><br />';
				
				echo '<div id="soliloquy-lightbox-image-desc" class="lightbox-desc"><p>' . Tgmsp_Lightbox_Strings::get_instance()->strings['meta_image_desc'] . '</p></div>';
				
				echo '<div id="soliloquy-lightbox-flash-desc" class="lightbox-desc"><p>';
					echo '<label for="soliloquy-lightbox-flash-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['flash_width'] . '</label>';
					echo '<input id="soliloquy-lightbox-flash-width-' . $attachment->ID . '" class="soliloquy-lightbox-flash-width" type="text" name="_soliloquy_lightbox[flash_width]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_width', true ) ) . '" /><br />';
					echo '<label for="soliloquy-lightbox-flash-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['flash_height'] . '</label>';
					echo '<input id="soliloquy-lightbox-flash-height-' . $attachment->ID . '" class="soliloquy-lightbox-flash-height" type="text" name="_soliloquy_lightbox[flash_height]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_flash_height', true ) ) . '" /></p>';
					echo '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
				echo '</div>';
				
				echo '<div id="soliloquy-lightbox-qt-desc" class="lightbox-desc"><p>';
					echo '<label for="soliloquy-lightbox-qt-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['movie_width'] . '</label>';
					echo '<input id="soliloquy-lightbox-qt-width-' . $attachment->ID . '" class="soliloquy-lightbox-qt-width" type="text" name="_soliloquy_lightbox[qt_width]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_width', true ) ) . '" /><br />';
					echo '<label for="soliloquy-lightbox-qt-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['movie_height'] . '</label>';
					echo '<input id="soliloquy-lightbox-qt-height-' . $attachment->ID . '" class="soliloquy-lightbox-qt-height" type="text" name="_soliloquy_lightbox[qt_height]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_qt_height', true ) ) . '" /></p>';
					echo '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
				echo '</div>';
				
				echo '<div id="soliloquy-lightbox-iframe-desc" class="lightbox-desc"><p>';
					echo '<label for="soliloquy-lightbox-iframe-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_width'] . '</label>';
					echo '<input id="soliloquy-lightbox-iframe-width-' . $attachment->ID . '" class="soliloquy-lightbox-iframe-width" type="text" name="_soliloquy_lightbox[iframe_width]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_width', true ) ) . '" /><br />';
					echo '<label for="soliloquy-lightbox-iframe-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['iframe_height'] . '</label>';
					echo '<input id="soliloquy-lightbox-iframe-height-' . $attachment->ID . '" class="soliloquy-lightbox-iframe-height" type="text" name="_soliloquy_lightbox[iframe_height]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_iframe_height', true ) ) . '" /></p>';
					echo '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
				echo '</div>';
				
				echo '<div id="soliloquy-lightbox-inline-desc" class="lightbox-desc"><p>' . Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_one'] . '</p><p><strong>' . Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_two'] . '</strong></p><p>' . Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_three'] . '</p><p><strong>' . htmlentities( Tgmsp_Lightbox_Strings::get_instance()->strings['inline_desc_four'] ) . '</strong></p></div>';
				
				echo '<div id="soliloquy-lightbox-ajax-desc" class="lightbox-desc"><p>';
					echo '<label for="soliloquy-lightbox-ajax-width-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_width'] . '</label>';
					echo '<input id="soliloquy-lightbox-ajax-width-' . $attachment->ID . '" class="soliloquy-lightbox-ajax-width" type="text" name="_soliloquy_lightbox[ajax_width]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_width', true ) ) . '" /><br />';
					echo '<label for="soliloquy-lightbox-ajax-height-' . $attachment->ID . '">' . Tgmsp_Lightbox_Strings::get_instance()->strings['ajax_height'] . '</label>';
					echo '<input id="soliloquy-lightbox-ajax-height-' . $attachment->ID . '" class="soliloquy-lightbox-ajax-height" type="text" name="_soliloquy_lightbox[ajax_height]" value="' . esc_attr( get_post_meta( $attachment->ID, '_soliloquy_lightbox_ajax_height', true ) ) . '" /></p>';
					echo '<p><em>' . Tgmsp_Lightbox_Strings::get_instance()->strings['content_settings'] . '</em></p>';
				echo '</div>';
			echo '</td>';
		echo '</tr>';
		
	}
		
	/**
	 * Saves the lightbox entry when the user clicks to save meta.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_var $_POST array keys sent when saving image meta
	 */
	public function save_meta( $post_var ) {
		
		$attachment_id = absint( $post_var['attach'] );

		update_post_meta( $attachment_id, '_soliloquy_image_lightbox', ( 'true' == $post_var['soliloquy-lightbox'] ) ? 1 : 0 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_content', preg_replace( '#[^a-z0-9-_]#', '', $post_var['soliloquy-lightbox-content'] ) );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_flash_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-flash-width'] ) ) ? trim( $post_var['soliloquy-lightbox-flash-width'] ) : 500 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_flash_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-flash-height'] ) ) ? trim( $post_var['soliloquy-lightbox-flash-height'] ) : 344 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_qt_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-qt-width'] ) ) ? trim( $post_var['soliloquy-lightbox-qt-width'] ) : 500 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_qt_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-qt-height'] ) ) ? trim( $post_var['soliloquy-lightbox-qt-height'] ) : 344 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_iframe_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-iframe-width'] ) ) ? trim( $post_var['soliloquy-lightbox-iframe-width'] ) : 500 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_iframe_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-iframe-height'] ) ) ? trim( $post_var['soliloquy-lightbox-iframe-height'] ) : 344 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_ajax_width', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-ajax-width'] ) ) ? trim( $post_var['soliloquy-lightbox-ajax-width'] ) : 500 );
		update_post_meta( $attachment_id, '_soliloquy_lightbox_ajax_height', preg_match( '|^\d+%{0,1}$|', trim( $post_var['soliloquy-lightbox-ajax-height'] ) ) ? trim( $post_var['soliloquy-lightbox-ajax-height'] ) : 344 );
		
		do_action( 'tgmsp_lightbox_update_meta', $attachment_id, $post_var );
		
	}
	
	/**
	 * Default types of lightbox content that can be filtered.
	 *
	 * @since 1.0.0
	 */
	public function types() {
	
		return apply_filters( 'tgmsp_lightbox_content_types', array( 'flash', 'quicktime', 'iframe', 'inline', 'ajax' ) );
	
	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {
	
		return self::$instance;
	
	}
	
}
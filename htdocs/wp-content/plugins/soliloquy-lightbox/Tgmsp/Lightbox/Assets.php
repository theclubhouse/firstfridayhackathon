<?php
/**
 * Aseets class for the Soliloquy Lightbox Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Lightbox
 * @author	Thomas Griffin
 */
class Tgmsp_Lightbox_Assets {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Lightbox::soliloquy_is_not_active() )
			return;

		/** Load dev scripts and styles if in Soliloquy dev mode */
		$dev = defined( 'SOLILOQUY_DEV' ) && SOLILOQUY_DEV ? '-dev' : '';

		/** Register scripts and styles */
		wp_register_script( 'soliloquy-lightbox-admin', plugins_url( 'js/admin' . $dev . '.js', dirname( dirname( __FILE__ ) ) ), array( 'jquery' ), Tgmsp_Lightbox::get_instance()->version, true );
		wp_register_script( 'soliloquy-lightbox', plugins_url( 'js/jquery.prettyPhoto.js', dirname( dirname( __FILE__ ) ) ), array( 'jquery' ), Tgmsp_Lightbox::get_instance()->version, true );
		wp_register_style( 'soliloquy-lightbox-admin', plugins_url( 'css/admin' . $dev . '.css', dirname( dirname( __FILE__ ) ) ), array(), Tgmsp_Lightbox::get_instance()->version );
		wp_register_style( 'soliloquy-lightbox', plugins_url( 'css/prettyPhoto.css', dirname( dirname( __FILE__ ) ) ), array(), Tgmsp_Lightbox::get_instance()->version );

		/** Load all hooks and filters for the class */
		add_action( 'admin_enqueue_scripts', array( $this, 'load_admin_assets' ) );

	}

	/**
	 * Register admin assets for the lightbox addon.
	 *
	 * @since 1.0.0
	 *
	 * @global object $current_screen The current screen object
	 * @global int $id The current post ID
	 * @global object $post The current post object
	 */
	public function load_admin_assets() {

		global $current_screen, $id, $post;

		/** Only load for the Soliloquy post type add and edit screens */
		if ( 'soliloquy' == $current_screen->post_type && 'post' == $current_screen->base ) {
			/** Send the post ID along with our script */
			$post_id = ( null === $id ) ? $post->ID : $id;

			$args = apply_filters( 'tgmsp_lightbox_script_args', array(
				'id'		=> $post_id,
				'speed' 	=> 5000,
				'opacity' 	=> .80,
				'width' 	=> 500,
				'height' 	=> 344,
				'separator' => '/',
				'padding'	=> 20,
				'wmode'		=> 'opaque'
			) );

			wp_enqueue_script( 'soliloquy-lightbox-admin' );
			wp_localize_script( 'soliloquy-lightbox-admin', 'soliloquy_lightbox', $args );
			wp_enqueue_style( 'soliloquy-lightbox-admin' );
		}

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
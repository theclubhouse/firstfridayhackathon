<?php
/*
Plugin Name: Soliloquy Thumbnails Addon
Plugin URI: http://soliloquywp.com/
Description: Enables thumbnail navigation support for the Soliloquy for WordPress plugin.
Author: Thomas Griffin
Author URI: http://thomasgriffinmedia.com/
Version: 1.0.4.1
License: GNU General Public License v2.0 or later
License URI: http://www.opensource.org/licenses/gpl-license.php
Text Domain: soliloquy-thumbnails
Domain Path: /languages
*/

/*
	Copyright 2012  Thomas Griffin  (email : thomas@thomasgriffinmedia.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/** Load all of the necessary class files for the plugin */
spl_autoload_register( 'Tgmsp_Thumbnails::autoload' );

/**
 * Init class for the Thumbnails Addon for Soliloquy.
 *
 * @since 1.0.0
 *
 * @package Soliloquy Thumbnails
 * @author Thomas Griffin <thomas@thomasgriffinmedia.com>
 */
class Tgmsp_Thumbnails {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Holds a copy of the main plugin filepath.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private static $file = __FILE__;

	/**
	 * Current version of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $version = '1.0.4.1';

	/**
	 * Constructor. Hooks all interactions into correct areas to start
	 * the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Run a hook before the slider is loaded and pass the object */
		do_action_ref_array( 'tgmsp_thumbnails_init', array( $this ) );

		register_activation_hook( __FILE__, array( $this, 'activation' ) );

		/** Load the plugin in the init hook (set high priority to make sure it loads after Soliloquy is fully loaded) */
		add_action( 'init', array( $this, 'init' ), 20 );
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain') );

	}

	/**
	 * Activation hook. Checks to make sure that the main Soliloquy for
	 * WordPress plugin is active before proceeding.
	 *
	 * @since 1.0.0
	 */
	public function activation() {

		/** If the Tgmsp class doesn't exist, deactivate ourself and die */
		if ( ! class_exists( 'Tgmsp' ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( __( 'The Soliloquy for WordPress plugin must be active before you can activate this plugin.', 'soliloquy-thumbnails' ) );
		}

		/** If Soliloquy isn't the correct version, deactivate ourself and die */
		if ( version_compare( Tgmsp::get_instance()->version, '1.5.2', '<' ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( sprintf( __( 'The current version of Soliloquy for WordPress, <strong>%s</strong>, does not meet the required version of <strong>1.5.2</strong> to run this Addon. Please update Soliloquy to the latest version before activating this Addon.', 'soliloquy-thumbnails' ), Tgmsp::get_instance()->version ) );
		}

	}

	/**
	 * Loads the plugin updater and all the actions and
	 * filters for the class.
	 *
	 * @since 1.0.0
	 *
	 * @global array $soliloquy_license Soliloquy license data
	 */
	public function init() {

		/** Setup the license checker */
		global $soliloquy_license;

		/** Only process update if a key has been entered and updates are on */
		if ( is_admin() ) :
			if ( isset( $soliloquy_license['license'] ) ) {
				$args = array(
					'remote_url' 	=> 'http://soliloquywp.com/',
					'version' 		=> $this->version,
					'plugin_name'   => 'Soliloquy Thumbnails Addon',
					'plugin_slug' 	=> 'soliloquy-thumbnails',
					'plugin_path' 	=> plugin_basename( __FILE__ ),
					'plugin_url' 	=> WP_PLUGIN_URL . '/soliloquy-thumbnails',
					'time' 			=> 43200,
					'key' 			=> $soliloquy_license['license']
				);

				/** Instantiate the automatic plugin updater class */
				$tgmsp_thumbnails_updater = new Tgmsp_Updater( $args );
			}

			/** Instantiate all the necessary admin components of the plugin */
			$tgmsp_thumbnails_admin  = new Tgmsp_Thumbnails_Admin();
			$tgmsp_thumbnails_assets = new Tgmsp_Thumbnails_Assets();

			/** If the Preview Addon is available, load the Preview class */
			if ( class_exists( 'Tgmsp_Preview', false ) ) // Don't check the autoload stack for this instantiation
				$tgmsp_thumbnails_preview = new Tgmsp_Thumbnails_Preview();
		endif;

		/** Instantiate all the necessary components of the plugin */
		$tgmsp_thumbnails_shortcode = new Tgmsp_Thumbnails_Shortcode();
		$tgmsp_thumbnails_strings	= new Tgmsp_Thumbnails_Strings();

	}

	/**
	 * PSR-0 compliant autoloader to load classes as needed.
	 *
	 * @since 1.0.0
	 *
	 * @param string $classname The name of the class
	 * @return null Return early if the class name does not start with the correct prefix
	 */
	public static function autoload( $classname ) {

		if ( 'Tgmsp_Thumbnails' !== mb_substr( $classname, 0, 16 ) )
			return;

		$filename = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . str_replace( '_', DIRECTORY_SEPARATOR, $classname ) . '.php';
		if ( file_exists( $filename ) )
			require $filename;

	}

	/**
	 * Helper method to determine if Soliloquy is inactive or not.
	 *
	 * @since 1.0.0
	 *
	 * @global string $pagenow The current page slug
	 * @return bool True if Soliloquy is not active, false otherwise
	 */
	public static function soliloquy_is_not_active() {

		global $pagenow;

		return ! ( class_exists( 'Tgmsp' ) ) && ! ( isset( $_GET['action'] ) && 'do-plugin-upgrade' == $_GET['action'] || 'plugin-editor.php' == $pagenow && isset( $_REQUEST['file'] ) && preg_match( '|^soliloquy|', $_REQUEST['file'] ) || 'update-core.php' == $pagenow );

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 *
	 * @return object The class object instance
	 */
	public static function get_instance() {

		return self::$instance;

	}

	/**
	 * Getter method for retrieving the main plugin filepath.
	 *
	 * @since 1.0.0
	 *
	 * @return string The main plugin filepath
	 */
	public static function get_file() {

		return self::$file;

	}

	/**
	 * Load the plugin's textdomain hooked to 'plugins_loaded'.
	 *
	 * @since 1.5.7
	 */
	function load_plugin_textdomain() {

		load_plugin_textdomain( 'soliloquy-thumbnails', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	}

}

/** Instantiate the init class */
$tgmsp_thumbnails = new Tgmsp_Thumbnails();
/**
 * jQuery to power the thumbnails preview.
 *
 * @package   Tgmsp-Thumbnails
 * @version   1.0.0
 * @author    Thomas Griffin <thomas@thomasgriffinmedia.com>
 * @copyright Copyright (c) 2013, Thomas Griffin
 */
jQuery(document).ready(function($) {

	/** Append information to the global settings ID var */
	if ( 'undefined' == typeof soliloquyPreviewSettingsID || false == soliloquyPreviewSettingsID )
		soliloquyPreviewSettingsID = '#soliloquy_thumbnails_settings .form-table td,';
	else
		soliloquyPreviewSettingsID += '#soliloquy_thumbnails_settings .form-table td,';

});
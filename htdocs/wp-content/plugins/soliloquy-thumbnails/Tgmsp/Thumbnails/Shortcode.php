<?php
/**
 * Shortcode class for the Soliloquy Thumbnails Addon.
 *
 * @since 1.0.0
 *
 * @package Soliloquy Thumbnails
 * @author  Thomas Griffin
 */
class Tgmsp_Thumbnails_Shortcode {

    /**
     * Holds a copy of the object for easy reference.
     *
     * @since 1.0.0
     *
     * @var object
     */
    private static $instance;

    /**
     * Constructor. Hooks all interactions to initialize the class.
     *
     * @since 1.0.0
     */
    public function __construct() {

        self::$instance = $this;

        /** Return early if Soliloquy is not active or if we are in preview mode */
        if ( Tgmsp_Thumbnails::soliloquy_is_not_active() || defined( 'DOING_AJAX' ) && DOING_AJAX )
            return;

        add_filter( 'tgmsp_after_slider', array( $this, 'thumbnails' ), 100, 5 );
        add_action( 'tgmsp_before_slider_init', array( $this, 'thumbnails_init' ) );
        add_action( 'tgmsp_slider_script', array( $this, 'infuse_thumbnails' ) );

    }

    /**
	 * Adds an appropriate class if a thumbnail slider is active.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes Current slider classes
	 * @param int $id The current slider ID
	 * @return array $classes Amended array of slider classes
	 */
	public function classes( $classes, $id ) {

		/** Get the thumbnails data for the current slider */
		$meta = get_post_meta( $id, '_soliloquy_thumbnails', true );

		/** If no thumbnails are set, return our classes */
		if ( $this->is_not_thumbnails( $meta ) )
			return $classes;

		/** We know a carousel is being used, so let's add the class */
		$classes[] = 'soliloquy-thumbnails-active';
		$classes[] = 'soliloquy-thumbnails-container';

		/** Return the classes */
		return array_unique( $classes );

	}

    /**
     * Outputs the thumbnail navigation directly below the slider.
     *
     * @since 1.0.0
     *
     * @param string $slider String of slider HTML
     * @param int $id The current slider ID
     * @param array $images The images in the slider with their data
     * @param array $soliloquy_data Array of metadata about the slider
     * @param int $soliloquy_count The current index of the slider on the page
     * @return string $slider Slider with appended thumbnail navigation
     */
    public function thumbnails( $slider, $id, $images, $soliloquy_data, $soliloquy_count ) {

    	/** Get the Carousel data for the current slider */
		$meta = get_post_meta( $id, '_soliloquy_thumbnails', true );

		/** If we are not using thumbnails, return the slider HTML */
		if ( $this->is_not_thumbnails( $meta ) )
			return $slider;

        // If using the crop addon and cropping is checked, let's crop those thumbnails!
        if ( class_exists( 'Tgmsp_Crop', false ) && isset( $meta['crop'] ) && $meta['crop'] )
            $images = $this->get_cropped_thumbs( $images, $meta, $id );

        /** Set our counter, margin helper, distance and thumbnails position */
        $i = 1;
		$margin = isset( $meta['margin'] ) ? $meta['margin'] : apply_filters( 'tgmsp_thumbnails_default_thumb_margin', 5, $id );
		$distance = isset( $meta['distance'] ) ? absint( $meta['distance'] ) : 20;
		$position = isset( $meta['position'] ) ? $meta['position'] : 'bottom';
		$place	  = 'top' == $position ? 'bottom' : 'top';

		/** If a custom size is chosen, all image sizes will be cropped the same, so grab width/height from first image */
		$width 	= $soliloquy_data[absint( $soliloquy_count )]['meta']['width'] ? $soliloquy_data[absint( $soliloquy_count )]['meta']['width'] : $images[0]['width'];

		// If the width is zero, make sure we have a positive width value first.
		if ( 0 == $width || empty( $width ) ) {
			foreach ( $images as $image ) {
				if ( ! empty( $image['width'] ) && $image['width'] > 0 ) {
					$width = $image['width'];
					break;
				}
			}
		}

		$width	= $ratio_width = apply_filters( 'tgmsp_thumbnails_slider_width', $width, $id );
		$width	= preg_match( '|%$|', trim( $width ) ) ? trim( $width ) . ';' : absint( $width ) . 'px;';
		$height = $soliloquy_data[absint( $soliloquy_count )]['meta']['height'] ? $soliloquy_data[absint( $soliloquy_count )]['meta']['height'] : $images[0]['height'];

		// If the height is zero, make sure we have a positive height value first.
		if ( 0 == $height || empty( $height ) ) {
			foreach ( $images as $image ) {
				if ( ! empty( $image['height'] ) && $image['height'] > 0 ) {
					$height = $image['height'];
					break;
				}
			}
		}

		$height	= $ratio_height = apply_filters( 'tgmsp_thumbnails_slider_height', $height, $id );
		$height	= preg_match( '|%$|', trim( $height ) ) ? trim( $height ) . ';' : absint( $height ) . 'px;';

		// Add filter for thumbnails classes.
		add_filter( 'tgmsp_slider_classes', array( $this, 'classes' ), 10, 2 );

        $thumbs = '<div id="soliloquy-thumbnails-container-' . esc_attr( $id ) . '" ' . Tgmsp_Shortcode::get_instance()->get_custom_slider_classes() . '  style="margin-' . esc_attr( $place ) . ': ' . $distance . 'px; ' . apply_filters( 'tgmsp_thumbnails_slider_width_output', 'max-width: ' . $width, $width, $id ) . ' ' . apply_filters( 'tgmsp_thumbnails_slider_height_output', 'max-height: ' . $height, $height, $id ) . ' ' . apply_filters( 'tgmsp_thumbnails_slider_container_style', '', $id ) . '">';
            $thumbs .= '<div id="soliloquy-thumbnails-' . esc_attr( $id ) . '" class="soliloquy soliloquy-thumbnails">';
                $thumbs .= '<ul id="soliloquy-thumbnails-list-' . esc_attr( $id ) . '" class="soliloquy-slides soliloquy-thumbnails-slides">';
                    foreach ( $images as $image ) {
                        $alt    = empty( $image['alt'] ) ? apply_filters( 'tgmsp_thumbnails_no_alt', '', $id, $image ) : $image['alt'];
                        $title  = empty( $image['title'] ) ? apply_filters( 'tgmsp_thumbnails_no_title', '', $id, $image ) : $image['title'];
                        $last   = $i == count( $images ) ? 'soliloquy-last-item' : '';

                        $slide = '<li id="soliloquy-thumbnails-' . esc_attr( $id ) . '-item-' . $i . '" class="soliloquy-item soliloquy-thumbnails-item ' . $last . '" style="margin-right: ' . absint( $margin ) . 'px; ' . apply_filters( 'tgmsp_thumbnails_slider_item_style', 'display: none;', $id, $image, $i ) . '" ' . apply_filters( 'tgmsp_thumbnails_slider_item_attr', '', $id, $image, $i ) . '>';
                            /** Use data attributes to fake loading of the image until its time to get to it */
							if ( 0 !== $soliloquy_data[absint( $soliloquy_count )]['meta']['number'] && ( $i - 1 ) == $soliloquy_data[absint( $soliloquy_count )]['meta']['number'] )
								$slide .= apply_filters( 'tgmsp_thumbnails_image_output', '<img class="soliloquy-item-image soliloquy-thumbnails-item-image" src="' . esc_url( $image['src'] ) . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" />', $id, $image, $alt, $title );
							else if ( 1 == $i && 0 == $soliloquy_data[absint( $soliloquy_count )]['meta']['number'] )
								$slide .= apply_filters( 'tgmsp_thumbnails_image_output', '<img class="soliloquy-item-image soliloquy-thumbnails-item-image" src="' . esc_url( $image['src'] ) . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" />', $id, $image, $alt, $title );
							else
								$slide .= apply_filters( 'tgmsp_thumbnails_image_output', '<img class="soliloquy-item-image soliloquy-thumbnails-item-image" src="' . esc_url( plugins_url( 'soliloquy/css/images/holder.gif', dirname( Tgmsp::get_file() ) ) ) . '" data-soliloquy-src="' . esc_url( $image['src'] ) . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" />', $id, $image, $alt, $title );
                        $slide .= '</li>';
                        $thumbs .= apply_filters( 'tgmsp_thumbnails_individual_slide', $slide, $id, $image, $i );
                        $i++;
                    }
                $thumbs .= '</ul>';
                $thumbs = apply_filters( 'tgmsp_thumbnails_inside_slider', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );
            $thumbs .= '</div>';
            $thumbs = apply_filters( 'tgmsp_thumbnails_slider_container', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );
        $thumbs .= '</div>';

        $thumbs = apply_filters( 'tgmsp_thumbnails_after_slider', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );

        // If the placement is at the top, prepend the thumbnails, else append them to the slider.
        if ( 'top' == $position )
        	$slider = $thumbs . $slider;
        else
        	$slider = $slider . $thumbs;

        // Remove slider class filter.
        remove_filter( 'tgmsp_slider_classes', array( $this, 'classes' ), 10, 2 );

        return apply_filters( 'tgmsp_thumbnails_slider_shortcode', $slider, $id, $images );

    }

    /**
     * Since thumbnail navigation must be initialized before the actual slider
     * itself, we hook in before the original slider init script is output.
     *
     * @since 1.0.0
     *
     * @param array $slider Array of slider data including ID and meta
     */
    public function thumbnails_init( $slider ) {

    	/** Get the thumbnails data for the current slider */
		$meta = get_post_meta( $slider['id'], '_soliloquy_thumbnails', true );

		/** If we are not using thumbnails, return the slider HTML */
		if ( $this->is_not_thumbnails( $meta ) )
			return $slider;

        /** Setup variables for output */
        $css    = isset( $slider['meta']['css'] )    && $slider['meta']['css']   ? 'true' : 'false';
        $touch  = isset( $slider['meta']['touch'] )  && $slider['meta']['touch'] ? 'true' : 'false';
        $width  = isset( $meta['width'] ) ? $meta['width'] : apply_filters( 'tgmsp_thumbnails_default_thumb_width', 150, $slider['id'] );
        $margin = isset( $meta['margin'] ) ? $meta['margin'] : apply_filters( 'tgmsp_thumbnails_default_thumb_margin', 5, $slider['id'] );
        $min    = isset( $meta['minimum'] ) ? $meta['minimum'] : 3;
        $max	= isset( $meta['maximum'] ) ? $meta['maximum'] : 3;

        // If using the crop addon, set the width to the crop width.
        if ( class_exists( 'Tgmsp_Crop', false ) && isset( $meta['crop_width'] ) && $meta['crop_width'] )
            $width = $meta['crop_width'];

        /** Prepare the preloading script */
		$script = 'var soliloquy_holder_thumbs = $("#soliloquy-thumbnails-container-' . absint( $slider['id'] ) . '").find(".soliloquy-item img.soliloquy-item-image");';
		$script .= 'if(0 !== soliloquy_holder_thumbs.length){';
			$script .= '$.each(soliloquy_holder_thumbs, function(i,el){';
				$script .= 'if(typeof $(this).attr("data-soliloquy-src") == "undefined" || false == $(this).attr("data-soliloquy-src")) return;';
				$script .= '(new Image()).src = $(this).attr("data-soliloquy-src");';
				$script .= '$(this).attr("src", $(this).attr("data-soliloquy-src")).removeAttr("data-soliloquy-src");';
			$script .= '});';
		$script .= '}';

        ?>
        <script type="text/javascript">jQuery(document).ready(function($){<?php echo $script; ?>$('#soliloquy-thumbnails-<?php echo absint( $slider['id'] ); ?>').soliloquy({animation: 'slide',controlNav: false,animationLoop: false,slideshow: false,itemWidth: <?php echo absint( $width ); ?>,itemMargin: <?php echo absint( $margin ); ?>,minItems: <?php echo absint( $min ); ?>,maxItems: <?php echo absint( $max ); ?>,useCSS: <?php echo $css; ?>,touch: <?php echo $touch; ?>,namespace: 'soliloquy-',selector: '.soliloquy-thumbnails-slides > li',controlsContainer:'<?php echo apply_filters( 'tgmsp_thumbnails_slider_controls', '#soliloquy-thumbnails-container-' . absint( $slider['id'] ), $slider['id'] ); ?>',<?php do_action( 'tgmsp_thumbnails_slider_init', $slider, absint( $slider['id'] ) ); ?>asNavFor: '#soliloquy-<?php echo absint( $slider['id'] ); ?>',start:function(){$("#soliloquy-thumbnails-container-<?php echo absint( $slider['id'] ); ?>").css({ "background" : "transparent", "background-image" : "none", "height" : "auto" });<?php $this->height_check( $slider['id'] ); ?>}});});</script>
        <?php

    }

    /**
     * Infuse the sync property into the main slider to sync the slider
     * and thumbnail navigation.
     *
     * @since 1.0.0
     *
     * @param array $slider Array of slider data including ID and meta
     */
    public function infuse_thumbnails( $slider ) {

        echo 'sync: "#soliloquy-thumbnails-' . absint( $slider['id'] ) . '",';

    }

    /**
	 * Helper function to determine if the slider has thumbnails or not.
	 *
	 * @since 1.0.0
	 *
	 * @param array $meta The current meta for the slider
	 * @return bool True thumbnails are active, false if not
	 */
	public function is_not_thumbnails( $meta ) {

		return (bool) ( ! isset( $meta['use'] ) || isset( $meta['use'] ) && ! $meta['use'] );

	}

	/**
	 * Ensure that the height is set properly with the dynamic image loading.
	 *
	 * @since 1.0.3
	 */
	public function height_check( $id ) {

		echo 'var soliloquyPoll' . absint( $id ) . ' = (function(){var timer = 0;return function(callback, ms){clearInterval(timer);timer = setInterval(callback, ms);};})(), soliloquyFlag' . absint( $id ) . ' = false;';
        echo 'soliloquyPoll' . absint( $id ) . '(function(){ if ( ! soliloquyFlag' . absint( $id ) . ' ) { if ( 0 === $("#soliloquy-container-' . absint( $id ) . ' .soliloquy").height() ) {';
		        echo 'if ( 0 === $("#soliloquy-container-' . absint( $id ) . ' .soliloquy-item:first").height() ) return;';
                echo '$("#soliloquy-container-' . absint( $id ) . ' .soliloquy").height($("#soliloquy-container-' . absint( $id ) . ' .soliloquy-item:first").height()); soliloquyFlag' . absint( $id ) . ' = true;';
            echo '}}';
        echo '}, 100);';

	}

	/**
	 * Crop thumbnails to the dimensions specified in the settings.
	 *
	 * @since 1.0.3
	 *
	 * @param array $images Array of image data to process.
	 * @param array $meta Array of thumbnail meta.
	 * @param int $id The slider ID.
	 * @return array $images Return the images with cropped URLs.
	 */
	public function get_cropped_thumbs( $images, $meta, $id ) {

        // Prepare variables necessary for cropping.
    	$width    = ! empty( $meta['crop_width'] ) ? $meta['crop_width'] : apply_filters( 'tgmsp_thumbnails_crop_width', 100, $id );
    	$height   = ! empty( $meta['crop_height'] ) ? $meta['crop_height'] : apply_filters( 'tgmsp_thumbnails_crop_height', 75, $id );
    	$position = apply_filters( 'tgmsp_thumbnails_crop_position', 'c', $id );
    	$quality  = apply_filters( 'tgmsp_thumbnails_crop_quality', 100, $id );
    	$retina   = apply_filters( 'tgmsp_thumbnails_crop_retina', false, $id );

    	// Loop through each of the images and crop them.
    	foreach ( $images as $i => $image ) {
    	    // Skip over any HTML or video slides.
    	    if ( isset( $image['mime'] ) && 'image' !== $image['mime'] )
    	        continue;

        	$new_image = Tgmsp_Crop::resize_image( $image['src'], $width, $height, true, $position, $quality, $retina );

        	// If there is an error cropping the image, simply return the default image.
    		if ( is_wp_error( $new_image ) ) {
    		    // If debugging is defined, print out the error.
    		    if ( defined( 'SOLILOQUY_CROP_DEBUG' ) && SOLILOQUY_CROP_DEBUG )
        		    echo '<pre>' . print_r( $new_image->get_error_message(), true ) . '</pre>';

        		continue;
    		} else {
    		    $images[$i]['src'] = $new_image;
            }
    	}

    	return apply_filters( 'tgmsp_thumbnails_cropped_images', $images, $meta, $id );

	}

    /**
     * Getter method for retrieving the object instance.
     *
     * @since 1.0.0
     *
     * @return object The class object instance
     */
    public static function get_instance() {

        return self::$instance;

    }

}
<?php
/**
 * Admin class for the Soliloquy Thumbnails Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Thumbnails
 * @author	Thomas Griffin
 */
class Tgmsp_Thumbnails_Admin {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		add_action( 'admin_init', array( $this, 'deactivation' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_thumbnail_settings' ), 10, 2 );

	}

	/**
	 * Deactivate the plugin if Soliloquy is not active and update the recently
	 * activate plugins with our plugin.
	 *
	 * @since 1.0.0
	 */
	public function deactivation() {

		/** Don't deactivate when doing a Soliloquy update or when editing Soliloquy from the Plugin Editor */
		if ( Tgmsp_Thumbnails::soliloquy_is_not_active() ) {
			$recent = (array) get_option( 'recently_activated' );
			$recent[plugin_basename( Tgmsp_Thumbnails::get_file() )] = time();
			update_option( 'recently_activated', $recent );
			deactivate_plugins( plugin_basename( Tgmsp_Thumbnails::get_file() ) );
		}

	}

		/**
	 * Adds the Soliloquy Carousel metabox to the Soliloquy edit screen.
	 *
	 * @since 1.0.0
	 */
	public function add_meta_boxes() {

		add_meta_box( 'soliloquy_thumbnails_settings', Tgmsp_Thumbnails_Strings::get_instance()->strings['thumbnail_settings'], array( $this, 'thumbnail_settings' ), 'soliloquy', 'normal', 'core' );

	}

	/**
	 * Callback function for the Soliloquy Thumbnails metabox.
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object
	 */
	public function thumbnail_settings( $post ) {

		/** Always keep security first */
		wp_nonce_field( 'soliloquy_thumbnail_settings', 'soliloquy_thumbnail_settings' );

		do_action( 'tgmsp_thumbnails_before_settings_table', $post );

		?>
		<table class="form-table">
			<tbody>
				<?php do_action( 'tgmsp_thumbnails_before_setting_use', $post ); ?>
				<tr id="soliloquy-thumbnails-use-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-use"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['use_thumbnails']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-use" type="checkbox" name="_soliloquy_thumbnails[use]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'use' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'use' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['use_thumbnails_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_width', $post ); ?>
				<tr id="soliloquy-thumbnails-width-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-width"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_width']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-width" type="text" name="_soliloquy_thumbnails[width]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'width' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_width_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_margin', $post ); ?>
				<tr id="soliloquy-thumbnails-margin-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-margin"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_margin']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-margin" type="text" name="_soliloquy_thumbnails[margin]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'margin' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_margin_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_minimum', $post ); ?>
				<tr id="soliloquy-thumbnails-minimum-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-minimum"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_minimum']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-minimum" type="text" name="_soliloquy_thumbnails[minimum]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'minimum' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_minimum_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_maximum', $post ); ?>
				<tr id="soliloquy-thumbnails-maximum-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-maximum"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_maximum']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-maximum" type="text" name="_soliloquy_thumbnails[maximum]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'maximum' ) ); ?>" />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_maximum_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_position', $post ); ?>
				<tr id="soliloquy-thumbnails-position-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-position"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_position']; ?></label></th>
					<td>
						<?php
							$positions = apply_filters( 'tgmsp_thumbnails_positions', array( 'bottom', 'top' ) );
							echo '<select id="soliloquy-thumbnails-position" name="_soliloquy_thumbnails[position]">';
								foreach ( $positions as $position ) {
									echo '<option value="' . esc_attr( $position ) . '"' . selected( $position, Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'position' ), false ) . '>' . esc_html( $position ) . '</option>';
								}
							echo '</select>';
						?>
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_position_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_before_setting_distance', $post ); ?>
				<tr id="soliloquy-thumbnails-distance-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-distance"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_distance']; ?></label></th>
					<td>
						<?php
						if ( '' === Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'distance' ) ) { ?>
							<input id="soliloquy-thumbnails-distance" type="text" name="_soliloquy_thumbnails[distance]" value="20" /> <?php } else { ?>
							<input id="soliloquy-thumbnails-distance" type="text" name="_soliloquy_thumbnails[distance]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'distance' ) ); ?>" /> <?php } ?>
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['item_distance_desc']; ?></span>
					</td>
				</tr>
				<?php do_action( 'tgmsp_thumbnails_end_of_settings', $post ); ?>

				<?php if ( class_exists( 'Tgmsp_Crop', false ) ) : ?>
				<tr id="soliloquy-thumbnails-crop-box" valign="middle">
					<th scope="row"><label for="soliloquy-thumbnails-crop"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['crop']; ?></label></th>
					<td>
						<input id="soliloquy-thumbnails-crop" type="checkbox" name="_soliloquy_thumbnails[crop]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'crop' ) ); ?>" <?php checked( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'crop' ), 1 ); ?> />
						<span class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['crop_desc']; ?></span>
					</td>
				</tr>
				<tr id="soliloquy-thumbnails-crop-size-box" valign="middle">
				    <th scope="row"><label for="soliloquy-thumbnails-crop-width"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['crop_size']; ?></label></th>
				    <td>
				        <input id="soliloquy-thumbnails-crop-width" type="text" name="_soliloquy_thumbnails[crop_width]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'crop_width' ) ); ?>" /> &#215; <input id="soliloquy-thumbnails-crop-height" type="text" name="_soliloquy_thumbnails[crop_height]" value="<?php echo esc_attr( Tgmsp_Admin::get_custom_field( '_soliloquy_thumbnails', 'crop_height' ) ); ?>" />
						<p class="description"><?php echo Tgmsp_Thumbnails_Strings::get_instance()->strings['crop_size_desc']; ?></p>
				    </td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?php

		do_action( 'tgmsp_thumbnails_after_settings', $post );

	}

	/**
	 * Save thumbnail settings post meta fields added to Soliloquy metaboxes.
	 *
	 * @since 1.0.0
	 *
	 * @param int $post_id The post ID
	 * @param object $post Current post object data
	 */
	public function save_thumbnail_settings( $post_id, $post ) {

		/** Bail out if we fail a security check */
		if ( ! isset( $_POST[sanitize_key( 'soliloquy_thumbnail_settings' )] ) || ! wp_verify_nonce( $_POST[sanitize_key( 'soliloquy_thumbnail_settings' )], 'soliloquy_thumbnail_settings' ) )
			return $post_id;

		/** Bail out if running an autosave, ajax or a cron */
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			return;
		if ( defined( 'DOING_CRON' ) && DOING_CRON )
			return;

		/** Bail out if the user doesn't have the correct permissions to update the slider */
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;

		/** All security checks passed, so let's store our data */
		$settings = isset( $_POST['_soliloquy_thumbnails'] ) ? $_POST['_soliloquy_thumbnails'] : '';

		/** Sanitize all data before updating */
		$settings['use']		= isset( $_POST['_soliloquy_thumbnails']['use'] ) ? 1 : 0;
		$settings['width'] 		= isset( $_POST['_soliloquy_thumbnails']['width'] ) && ! empty( $_POST['_soliloquy_thumbnails']['width'] ) ? absint( $_POST['_soliloquy_thumbnails']['width'] ) : '';
		$settings['margin'] 	= isset( $_POST['_soliloquy_thumbnails']['margin'] ) && ! empty( $_POST['_soliloquy_thumbnails']['margin'] ) ? absint( $_POST['_soliloquy_thumbnails']['margin'] ) : '';
		$settings['minimum']	= isset( $_POST['_soliloquy_thumbnails']['minimum'] ) && ! empty( $_POST['_soliloquy_thumbnails']['minimum'] ) ? absint( $_POST['_soliloquy_thumbnails']['minimum'] ) : '';
		$settings['maximum'] 	= isset( $_POST['_soliloquy_thumbnails']['maximum'] ) && ! empty( $_POST['_soliloquy_thumbnails']['maximum'] ) ? absint( $_POST['_soliloquy_thumbnails']['maximum'] ) : '';
		$settings['position']	= preg_replace( '#[^a-z0-9-_]#', '', $_POST['_soliloquy_thumbnails']['position'] );
		$settings['distance']	= absint( $_POST['_soliloquy_thumbnails']['distance'] );

		if ( class_exists( 'Tgmsp_Crop', false ) ) {
    		$settings['crop']        = isset( $_POST['_soliloquy_thumbnails']['crop'] ) ? 1 : 0;
    		$settings['crop_width']  = ! empty( $_POST['_soliloquy_thumbnails']['crop_width'] ) ? absint( $_POST['_soliloquy_thumbnails']['crop_width'] ) : '';
    		$settings['crop_height'] = ! empty( $_POST['_soliloquy_thumbnails']['crop_height'] ) ? absint( $_POST['_soliloquy_thumbnails']['crop_height'] ) : '';
		}

		do_action( 'tgmsp_thumbnails_save_settings', $settings, $post_id, $post );

		/** Update post meta with sanitized values */
		update_post_meta( $post_id, '_soliloquy_thumbnails', $settings );

		// Go ahead and crop thumbs now to save bandwidth on the front-end.
        $base_meta = get_post_meta( $post_id, '_soliloquy_settings', true );
        $images    = Tgmsp_Shortcode::get_instance()->get_images( $post_id, $base_meta );
		if ( empty( $images ) )
			return;

        if ( class_exists( 'Tgmsp_Crop', false ) ) {
            // Prepare variables necessary for cropping.
            $thumbs_meta = get_post_meta( $post_id, '_soliloquy_thumbnails', true );
        	$width      = ! empty( $thumbs_meta['crop_width'] ) ? $thumbs_meta['crop_width'] : apply_filters( 'tgmsp_thumbnails_crop_width', 100, $post_id );
        	$height     = ! empty( $thumbs_meta['crop_height'] ) ? $thumbs_meta['crop_height'] : apply_filters( 'tgmsp_thumbnails_crop_height', 75, $post_id );
        	$position   = apply_filters( 'tgmsp_thumbnails_crop_position', 'c', $post_id );
        	$quality    = apply_filters( 'tgmsp_thumbnails_crop_quality', 100, $post_id );
        	$retina     = apply_filters( 'tgmsp_thumbnails_crop_retina', false, $post_id );

            // Loop through the images and generate the crop sizes.
            foreach ( $images as $image ) {
                if ( isset( $image['mime'] ) && 'image' !== $image['mime'] )
                    continue;

        		$new_image = Tgmsp_Crop::resize_image( $image['src'], $width, $height, true, $position, $quality, $retina );

        		// If there is an error cropping the image, simply return the default image.
        		if ( is_wp_error( $new_image ) ) {
        		    // If debugging is defined, print out the error.
        		    if ( defined( 'SOLILOQUY_CROP_DEBUG' ) && SOLILOQUY_CROP_DEBUG )
            		    echo '<pre>' . print_r( $new_image->get_error_message(), true ) . '</pre>';
            	}
            }
        }

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 *
	 * @return object The class object instance
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
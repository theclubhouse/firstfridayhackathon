<?php
/**
 * Preview class for the Soliloquy Thumbnails Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Thumbnails
 * @author	Thomas Griffin
 */
class Tgmsp_Thumbnails_Preview {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Thumbnails::soliloquy_is_not_active() )
			return;

		add_action( 'tgmsp_preview_start', array( $this, 'preview_init' ) );

	}

	/**
	 * Init callback to make sure that filters and hooks are only executed in the Preview
	 * context.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_var The $_POST data from the Ajax request
	 */
	public function preview_init( $post_var ) {

		/** Only execute if there is a lightbox instance to process */
		foreach ( $post_var as $key => $val ) {
			if ( 'soliloquy-thumbnails-use' == $key && 'true' == $val ) {
				add_filter( 'tgmsp_slider_classes', array( $this, 'classes' ), 10, 2 );
		        add_filter( 'tgmsp_after_slider', array( $this, 'thumbnails' ), 100, 6 );
		        add_action( 'tgmsp_before_slider_init', array( $this, 'thumbnails_init' ) );
		        add_action( 'tgmsp_slider_script', array( $this, 'infuse_thumbnails' ) );
				break;
			}
		}

	}

	/**
	 * Adds an appropriate class if a thumbnail slider is active.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes Current slider classes
	 * @param int $id The current slider ID
	 * @return array $classes Amended array of slider classes
	 */
	public function classes( $classes, $id ) {

		/** We know a carousel is being used, so let's add the class */
		$classes[] = 'soliloquy-thumbails-active';

		/** Return the classes */
		return array_unique( $classes );

	}

    /**
     * Outputs the thumbnail navigation directly below the slider.
     *
     * @since 1.0.0
     *
     * @param string $slider String of slider HTML
     * @param int $id The current slider ID
     * @param array $images The images in the slider with their data
     * @param array $soliloquy_data Array of metadata about the slider
     * @param int $soliloquy_count The current index of the slider on the page
     * @return string $slider Slider with appended thumbnail navigation
     */
    public function thumbnails( $slider, $id, $images, $soliloquy_data, $soliloquy_count, $post_var ) {

        /** Set our counter, margin helper, distance and thumbnails position */
        $i = 1;
		$margin = isset( $post_var['soliloquy-thumbnails-margin'] ) ? $post_var['soliloquy-thumbnails-margin'] : apply_filters( 'tgmsp_thumbnails_default_thumb_margin', 5 );
		$distance = isset( $post_var['soliloquy-thumbnails-distance'] ) ? absint( $post_var['soliloquy-thumbnails-distance'] ) : 20;
		$position = isset( $post_var['soliloquy-thumbnails-position'] ) ? $post_var['soliloquy-thumbnails-position'] : 'bottom';
		$place	  = 'top' == $position ? 'bottom' : 'top';

		/** If a custom size is chosen, all image sizes will be cropped the same, so grab width/height from first image */
		$width 	= $soliloquy_data[absint( $soliloquy_count )]['meta']['width'] ? $soliloquy_data[absint( $soliloquy_count )]['meta']['width'] : $images[0]['width'];

		// If the width is zero, make sure we have a positive width value first.
		if ( 0 == $width || empty( $width ) ) {
			foreach ( $images as $image ) {
				if ( ! empty( $image['width'] ) && $image['width'] > 0 ) {
					$width = $image['width'];
					break;
				}
			}
		}

		$width	= $ratio_width = apply_filters( 'tgmsp_thumbnails_slider_width', $width, $id );
		$width	= preg_match( '|%$|', trim( $width ) ) ? trim( $width ) . ';' : absint( $width ) . 'px;';
		$height = $soliloquy_data[absint( $soliloquy_count )]['meta']['height'] ? $soliloquy_data[absint( $soliloquy_count )]['meta']['height'] : $images[0]['height'];

		// If the height is zero, make sure we have a positive height value first.
		if ( 0 == $height || empty( $height ) ) {
			foreach ( $images as $image ) {
				if ( ! empty( $image['height'] ) && $image['height'] > 0 ) {
					$height = $image['height'];
					break;
				}
			}
		}

		$height	= $ratio_height = apply_filters( 'tgmsp_thumbnails_slider_height', $height, $id );
		$height	= preg_match( '|%$|', trim( $height ) ) ? trim( $height ) . ';' : absint( $height ) . 'px;';

        $thumbs = '<div id="soliloquy-thumbnails-container-' . esc_attr( $id ) . '" class="soliloquy-container soliloquy-thumbnails-container" style="margin-' . esc_attr( $place ) . ': ' . $distance . 'px; ' . apply_filters( 'tgmsp_thumbnails_slider_width_output', 'max-width: ' . $width, $width, $id ) . ' ' . apply_filters( 'tgmsp_thumbnails_slider_height_output', 'max-height: ' . $height, $height, $id ) . ' ' . apply_filters( 'tgmsp_thumbnails_slider_container_style', '', $id ) . '">';
            $thumbs .= '<div id="soliloquy-thumbnails-' . esc_attr( $id ) . '" class="soliloquy soliloquy-thumbnails">';
                $thumbs .= '<ul id="soliloquy-thumbnails-list-' . esc_attr( $id ) . '" class="soliloquy-slides soliloquy-thumbnails-slides">';
                    foreach ( $images as $image ) {
                        $alt    = empty( $image['alt'] ) ? apply_filters( 'tgmsp_thumbnails_no_alt', '', $id, $image ) : $image['alt'];
                        $title  = empty( $image['title'] ) ? apply_filters( 'tgmsp_thumbnails_no_title', '', $id, $image ) : $image['title'];
                        $last   = $i == count( $images ) ? 'soliloquy-last-item' : '';

                        $slide = '<li id="soliloquy-thumbnails-' . esc_attr( $id ) . '-item-' . $i . '" class="soliloquy-item soliloquy-thumbnails-item ' . $last . '" style="margin-right: ' . absint( $margin ) . 'px; ' . apply_filters( 'tgmsp_thumbnails_slider_item_style', 'display: none;', $id, $image, $i ) . '" ' . apply_filters( 'tgmsp_thumbnails_slider_item_attr', '', $id, $image, $i ) . '>';
                            $slide .= apply_filters( 'tgmsp_thumbnails_image_output', '<img class="soliloquy-item-image soliloquy-thumbnails-item-image" src="' . esc_url( $image['src'] ) . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" />', $id, $image, $alt, $title );
                        $slide .= '</li>';
                        $thumbs .= apply_filters( 'tgmsp_thumbnails_individual_slide', $slide, $id, $image, $i );
                        $i++;
                    }
                $thumbs .= '</ul>';
                $thumbs = apply_filters( 'tgmsp_thumbnails_inside_slider', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );
            $thumbs .= '</div>';
            $thumbs = apply_filters( 'tgmsp_thumbnails_slider_container', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );
        $thumbs .= '</div>';

        $thumbs = apply_filters( 'tgmsp_thumbnails_after_slider', $thumbs, $id, $images, $soliloquy_data, absint( $soliloquy_count ) );

        // If the placement is at the top, prepend the thumbnails, else append them to the slider.
        if ( 'top' == $position )
        	$slider = $thumbs . $slider;
        else
        	$slider = $slider . $thumbs;

        return apply_filters( 'tgmsp_thumbnails_slider_shortcode', $slider, $id, $images );

    }

    /**
     * Since thumbnail navigation must be initialized before the actual slider
     * itself, we hook in before the original slider init script is output.
     *
     * @since 1.0.0
     *
     * @param array $post_var Array of slider data including ID and meta
     */
    public function thumbnails_init( $slider ) {

        /** Setup variables for output */
        $css    = isset( $slider['soliloquy-slider-css'] ) && 'true' == $slider['soliloquy-slider-css'] ? 'true' : 'false';
        $touch  = isset( $slider['soliloquy-touch'] ) && 'true' == $slider['soliloquy-touch'] ? 'true' : 'false';
        $width  = isset( $slider['soliloquy-thumbnails-width'] ) ? $slider['soliloquy-thumbnails-width'] : apply_filters( 'tgmsp_thumbnails_default_thumb_width', 150 );
        $margin = isset( $slider['soliloquy-thumbnails-margin'] ) ? $slider['soliloquy-thumbnails-margin'] : apply_filters( 'tgmsp_thumbnails_default_thumb_margin', 5 );
        $min    = isset( $slider['soliloquy-thumbnails-minimum'] ) ? $slider['soliloquy-thumbnails-minimum'] : 3;
        $max	= isset( $slider['soliloquy-thumbnails-maximum'] ) ? $slider['soliloquy-thumbnails-maximum'] : 3;

        ?>
        <script type="text/javascript">jQuery('#soliloquy-thumbnails-<?php echo absint( $slider['id'] ); ?>').soliloquy({animation: 'slide',controlNav: false,animationLoop: false,slideshow: false,itemWidth: <?php echo absint( $width ); ?>,itemMargin: <?php echo absint( $margin ); ?>,minItems: <?php echo absint( $min ); ?>,maxItems: <?php echo absint( $max ); ?>,useCSS: <?php echo $css; ?>,touch: <?php echo $touch; ?>,namespace: 'soliloquy-',selector: '.soliloquy-thumbnails-slides > li',<?php do_action( 'tgmsp_thumbnails_slider_init', $slider, absint( $slider['id'] ) ); ?>asNavFor: '#soliloquy-preview-wrap #soliloquy-<?php echo absint( $slider['id'] ); ?>'});</script>
        <?php

    }

    /**
     * Infuse the sync property into the main slider to sync the slider
     * and thumbnail navigation.
     *
     * @since 1.0.0
     *
     * @param array $post_var Array of slider data including ID and meta
     */
    public function infuse_thumbnails( $post_var ) {

        echo 'sync: "#soliloquy-thumbnails-' . absint( $post_var['id'] ) . '",';

    }

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 *
	 * @return object The class object instance
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
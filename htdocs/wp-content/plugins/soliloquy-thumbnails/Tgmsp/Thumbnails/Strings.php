<?php
/**
 * Strings class for the Soliloquy Thumbnails Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Thumbnails
 * @author	Thomas Griffin
 */
class Tgmsp_Thumbnails_Strings {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Holds a copy of all the strings used by the Soliloquy Thumbnails Addon.
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	public $strings = array();

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_Thumbnails::soliloquy_is_not_active() )
			return;

		$this->strings = apply_filters( 'tgmsp_thumbnails_strings', array(
		    'crop'                  => __( 'Crop Slider Thumbnails?', 'soliloquy-thumbnails' ),
		    'crop_desc'             => __( 'Crops the slider thumbnails to the dimensions specified below.', 'soliloquy-thumbnails' ),
		    'crop_size'             => __( 'Crop Dimensions', 'soliloquy-thumbnails' ),
		    'crop_size_desc'        => __( 'Set the width and height for the thumbnails to be cropped.', 'soliloquy-thumbnails' ),
			'thumbnail_settings'	=> __( 'Soliloquy Thumbnail Settings', 'soliloquy-thumbnails' ),
			'item_distance'			=> __( 'Margin Between Slider', 'soliloquy-thumbnails' ),
			'item_distance_desc'	=> __( 'The margin (in pixels) between the slider and thumbnails.', 'soliloquy-thumbnails' ),
			'item_margin'			=> __( 'Individual Thumbnail Margin', 'soliloquy-thumbnails' ),
			'item_margin_desc'		=> __( 'Sets the margin between each thumbnail.', 'soliloquy-thumbnails' ),
			'item_maximum'			=> __( 'Maximum Visible Thumbnails', 'soliloquy-thumbnails' ),
			'item_maximum_desc'		=> __( 'The maximum number of thumbnails visible.', 'soliloquy-thumbnails' ),
			'item_minimum'			=> __( 'Minimum Visible Thumbnails', 'soliloquy-thumbnails' ),
			'item_minimum_desc'		=> __( 'The minimum number of thumbnails visible.', 'soliloquy-thumbnails' ),
			'item_move'				=> __( 'Number of Thumbnails to Move', 'soliloquy-thumbnails' ),
			'item_move_desc'		=> __( 'Sets the number of thumbnails moved with each animation.', 'soliloquy-thumbnails' ),
			'item_position'			=> __( 'Thumbnails Position', 'soliloquy-thumbnails' ),
			'item_position_desc'	=> __( 'Where to render the thumbnails (above or below the slider).', 'soliloquy-thumbnails' ),
			'item_width'			=> __( 'Individual Thumbnail Width', 'soliloquy-thumbnails' ),
			'item_width_desc' 		=> __( 'Sets the width (in pixels) of each thumbnail.', 'soliloquy-thumbnails' ),
			'use_thumbnails'		=> __( 'Use Thumbnail Navigation?', 'soliloquy-thumbnails' ),
			'use_thumbnails_desc'	=> __( 'Check here to enable or disable thumbnail navigation.', 'soliloquy-thumbnails' )
		) );

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 *
	 * @return object The class object instance
	 */
	public static function get_instance() {

		return self::$instance;

	}

}
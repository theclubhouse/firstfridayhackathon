<?php
/**
 * Admin class for the Soliloquy Crop Addon.
 *
 * @since 1.0.0
 *
 * @package Soliloquy Filters
 * @author  Thomas Griffin
 */
class Tgmsp_Crop_Admin {

    /**
     * Holds a copy of the object for easy reference.
     *
     * @since 1.0.0
     *
     * @var object
     */
    private static $instance;

    /**
     * Holds the menu pagehook string.
     *
     * @since 1.0.0
     *
     * @var bool|string
     */
    private $menu_hook = false;

    /**
     * Constructor. Hooks all interactions to initialize the class.
     *
     * @since 1.0.0
     */
    public function __construct() {

        self::$instance = $this;

        add_action( 'admin_init', array( $this, 'deactivation' ) );
        add_filter( 'tgmsp_default_sizes', array( $this, 'crop_size' ) );
        add_filter( 'tgmsp_slider_settings', array( $this, 'preserve_image_size' ), 10, 3 );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
        add_action( 'save_post', array( $this, 'save_crop_settings' ), 10, 2 );
        add_action( 'delete_attachment', array( $this, 'delete_cropped_image' ) );

    }

    /**
     * Deactivate the plugin if Soliloquy is not active and update the recently
     * activate plugins with our plugin.
     *
     * @since 1.0.0
     */
    public function deactivation() {

        /** Don't deactivate when doing a Soliloquy update or when editing Soliloquy from the Plugin Editor */
        if ( Tgmsp_Crop::soliloquy_is_not_active() ) {
            $recent = (array) get_option( 'recently_activated' );
            $recent[plugin_basename( Tgmsp_Crop::get_file() )] = time();
            update_option( 'recently_activated', $recent );
            deactivate_plugins( plugin_basename( Tgmsp_Crop::get_file() ) );
        }

    }

    /**
     * Filters the default slider sizes to add a new size for the Crop
     * Addon - "cropped".
     *
     * @since 1.0.0
     *
     * @param array $sizes Default slider sizes
     * @return array $sizes Amended array of slider sizes with "cropped" added
     */
    public function crop_size( $sizes ) {

        $sizes[] = 'cropped';
        return $sizes;

    }

    /**
     * If the "cropped" option is chosen, the custom slider size dimensions are
     * preserved for Crop resizing.
     *
     * @since 1.0.0
     *
     * @param array $settings Default Soliloquy meta settings
     * @param object $post The current post object
     * @param int $post_id The current post ID
     * @return array $settings Amended settings with size preserved
     */
    public function preserve_image_size( $settings, $post, $post_id ) {

        // Return early if the "cropped" size has not been chosen.
        if ( isset( $settings['default'] ) && 'cropped' != $settings['default'] )
            return $settings;

        // Preserve user selected image sizes.
        $settings['width']  = preg_match( '|^\d+%{0,1}$|', trim( $_POST['_soliloquy_settings']['width'] ) ) ? trim( $_POST['_soliloquy_settings']['width'] ) : 600;
        $settings['height'] = preg_match( '|^\d+%{0,1}$|', trim( $_POST['_soliloquy_settings']['height'] ) ) ? trim( $_POST['_soliloquy_settings']['height'] ) : 300;
        return $settings;

    }

    /**
     * Adds the Soliloquy Crop metabox to the Soliloquy edit screen.
     *
     * @since 1.0.0
     */
    public function add_meta_boxes() {

        add_meta_box( 'soliloquy_crop_settings', Tgmsp_Crop_Strings::get_instance()->strings['crop_settings'], array( $this, 'crop_settings' ), 'soliloquy', 'normal', 'core' );

    }

    /**
     * Callback function for the Soliloquy Crop metabox.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object
     */
    public function crop_settings( $post ) {

        /** Always keep security first */
        wp_nonce_field( 'soliloquy_crop_settings', 'soliloquy_crop_settings' );

        do_action( 'tgmsp_crop_before_settings_table', $post );

        ?>
        <table class="form-table">
            <tbody>
                <?php do_action( 'tgmsp_crop_before_setting_crop_position', $post ); ?>
                <tr id="soliloquy-crop-position-box" valign="middle">
                    <th scope="row"><label for="soliloquy-crop-position"><?php echo Tgmsp_Crop_Strings::get_instance()->strings['crop_position']; ?></label></th>
                    <td>
                        <?php
                            $positions = $this->crop_positions();
                            echo '<select id="soliloquy-crop-position" name="_soliloquy_crop[crop_position]">';
                            foreach ( (array) $positions as $array => $data )
                                echo '<option value="' . esc_attr( $data['type'] ) . '"' . selected( $data['type'], Tgmsp_Admin::get_custom_field( '_soliloquy_crop', 'crop_position' ), false ) . '>' . esc_html( $data['name'] ) . '</option>';
                            echo '</select>';
                        ?>
                        <span class="description"><?php echo Tgmsp_Crop_Strings::get_instance()->strings['crop_desc']; ?></span>
                    </td>
                </tr>
                <?php do_action( 'tgmsp_crop_end_of_settings', $post ); ?>
            </tbody>
        </table>
        <?php

        do_action( 'tgmsp_crop_after_settings', $post );

    }

    /**
     * Save crop settings post meta fields added to Soliloquy metaboxes.
     *
     * @since 1.0.0
     *
     * @param int $post_id The post ID
     * @param object $post Current post object data
     */
    public function save_crop_settings( $post_id, $post ) {

        /** Bail out if we fail a security check */
        if ( ! isset( $_POST[sanitize_key( 'soliloquy_crop_settings' )] ) || ! wp_verify_nonce( $_POST[sanitize_key( 'soliloquy_crop_settings' )], 'soliloquy_crop_settings' ) )
            return $post_id;

        /** Bail out if running an autosave, ajax or a cron */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
            return;
        if ( defined( 'DOING_CRON' ) && DOING_CRON )
            return;

        /** Bail out if the user doesn't have the correct permissions to update the slider */
        if ( ! current_user_can( 'edit_post', $post_id ) )
            return $post_id;

        /** All security checks passed, so let's store our data */
        $settings = isset( $_POST['_soliloquy_crop'] ) ? $_POST['_soliloquy_crop'] : '';

        /** Sanitize all data before updating */
        $settings['crop_position'] = preg_replace( '#[^a-z0-9-_]#', '', $_POST['_soliloquy_crop']['crop_position'] );

        do_action( 'tgmsp_crop_save_settings', $settings, $post_id, $post );

        /** Update post meta with sanitized values */
        update_post_meta( $post_id, '_soliloquy_crop', $settings );

        // Attempt to generate image sizes now to reduce overhead on front end.
        $base_meta = get_post_meta( $post_id, '_soliloquy_settings', true );
        $images    = Tgmsp_Shortcode::get_instance()->get_images( $post_id, $base_meta );
		if ( empty( $images ) )
			return;

        // Loop through the images and generate the crop sizes.
        foreach ( $images as $image ) {
            if ( isset( $image['mime'] ) && 'image' !== $image['mime'] )
                continue;

            $url		= $image['src'];
    		$args		= apply_filters( 'tgmsp_crop_image_args', array(
    			'src' 	=> esc_url( $url ),
    			'a'		=> $_POST['_soliloquy_crop']['crop_position'],
    			'w'		=> isset( $base_meta['width'] )  ? $base_meta['width']  : 600,
    			'h'		=> isset( $base_meta['height'] ) ? $base_meta['height'] : 300,
    			'q'		=> 100,
    			'r'     => false // Filter to true to allow for retina images.
    		) );
    		$new_image = Tgmsp_Crop::resize_image( $args['src'], $args['w'], $args['h'], true, $args['a'], $args['q'], $args['r'] );

    		// If there is an error cropping the image, simply return the default image.
    		if ( is_wp_error( $new_image ) ) {
    		    // If debugging is defined, print out the error.
    		    if ( defined( 'SOLILOQUY_CROP_DEBUG' ) && SOLILOQUY_CROP_DEBUG )
        		    echo '<pre>' . print_r( $new_image->get_error_message(), true ) . '</pre>';
        	}
        }

    }

    /**
     * Removes any extra cropped images when an attachment is deleted.
     *
     * @since 1.0.3
     *
     * @param int $post_id The post ID
     * @return null Return early if the appropriate metadata cannot be retrieved.
     */
    public function delete_cropped_image( $post_id ) {

        // Get attachment image metadata.
        $metadata = wp_get_attachment_metadata( $post_id );

        // Return if no metadata is found.
        if ( ! $metadata )
            return;

        // Return if we don't have the proper metadata
        if ( ! isset( $metadata['file'] ) || ! isset( $metadata['image_meta']['resized_images'] ) )
            return;

        // Grab the necessary info to removed the cropped images.
        $wp_upload_dir  = wp_upload_dir();
        $pathinfo       = pathinfo( $metadata['file'] );
        $resized_images = $metadata['image_meta']['resized_images'];

        // Loop through and deleted and resized/cropped images.
        foreach ( $resized_images as $dims ) {
            // Get the resized images filename and delete the image.
            $file = $wp_upload_dir['basedir'] . '/' . $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '-' . $dims . '.' . $pathinfo['extension'];

            // Delete the resized image.
            if ( is_user_logged_in() )
                unlink( $file );
            else
                @unlink( $file );
        }

    }

    /**
     * Default Crop crop positions that can be filtered.
     *
     * @since 1.0.0
     */
    public function crop_positions() {

        $positions = array(
            array(
                'type'  => 'c',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['center']
            ),
            array(
                'type'  => 't',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['top']
            ),
            array(
                'type'  => 'tr',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['top_right']
            ),
            array(
                'type'  => 'tl',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['top_left']
            ),
            array(
                'type'  => 'b',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['bottom']
            ),
            array(
                'type'  => 'br',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['bottom_right']
            ),
            array(
                'type'  => 'bl',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['bottom_left']
            ),
            array(
                'type'  => 'l',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['left']
            ),
            array(
                'type'  => 'r',
                'name'  => Tgmsp_Crop_Strings::get_instance()->strings['right']
            )
        );

        return apply_filters( 'tgmsp_crop_crop_positions', $positions );

    }

    /**
     * Getter method for retrieving the object instance.
     *
     * @since 1.0.0
     */
    public static function get_instance() {

        return self::$instance;

    }

}
<?php
/*
Plugin Name: Soliloquy Crop Addon
Plugin URI: http://soliloquywp.com/
Description: Enables image cropping and resizing support for the Soliloquy for WordPress plugin.
Author: Thomas Griffin
Author URI: http://thomasgriffinmedia.com/
Version: 1.0.3.1
License: GNU General Public License v2.0 or later
License URI: http://www.opensource.org/licenses/gpl-license.php
*/

/*
    Copyright 2013  Thomas Griffin  (email : thomas@thomasgriffinmedia.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/** Load all of the necessary class files for the plugin */
spl_autoload_register( 'Tgmsp_Crop::autoload' );

/**
 * Init class for the Crop Addon for Soliloquy.
 *
 * @since 1.0.0
 *
 * @package Tgmsp-Crop
 * @author Thomas Griffin <thomas@thomasgriffinmedia.com>
 */
class Tgmsp_Crop {

    /**
     * Holds a copy of the object for easy reference.
     *
     * @since 1.0.0
     *
     * @var object
     */
    private static $instance;

    /**
     * Holds a copy of the main plugin filepath.
     *
     * @since 1.0.0
     *
     * @var string
     */
    private static $file = __FILE__;

    /**
     * Current version of the plugin.
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $version = '1.0.3.1';

    /**
     * Constructor. Hooks all interactions into correct areas to start
     * the class.
     *
     * @since 1.0.0
     */
    public function __construct() {

        self::$instance = $this;

        /** Run a hook before the slider is loaded and pass the object */
        do_action_ref_array( 'tgmsp_crop_init', array( $this ) );

        register_activation_hook( __FILE__, array( $this, 'activation' ) );

        /** Load the plugin in the init hook (set high priority to make sure it loads after Soliloquy is fully loaded) */
        add_action( 'init', array( $this, 'init' ), 20 );

    }

    /**
     * Activation hook. Checks to make sure that the main Soliloquy for
     * WordPress plugin is active before proceeding.
     *
     * @since 1.0.0
     */
    public function activation() {

        /** If the Tgmsp class doesn't exist, deactivate ourself and die */
        if ( ! class_exists( 'Tgmsp' ) ) {
            deactivate_plugins( plugin_basename( __FILE__ ) );
            wp_die( __( 'The Soliloquy for WordPress plugin must be active before you can activate this plugin.', 'soliloquy-crop' ) );
        }

        /** If Soliloquy isn't the correct version, deactivate ourself and die */
        if ( version_compare( Tgmsp::get_instance()->version, '1.4.8.1', '<' ) ) {
            deactivate_plugins( plugin_basename( __FILE__ ) );
            wp_die( sprintf( __( 'The current version of Soliloquy for WordPress, <strong>%s</strong>, does not meet the required version of <strong>1.4.8.1</strong> to run this Addon. Please update Soliloquy to the latest version before activating this Addon.', 'soliloquy-crop' ), Tgmsp::get_instance()->version ) );
        }

    }

    /**
     * Loads the plugin updater and all the actions and
     * filters for the class.
     *
     * @since 1.0.0
     *
     * @global array $soliloquy_license Soliloquy license data
     */
    public function init() {

        /** Load the plugin textdomain for internationalizing strings */
        load_plugin_textdomain( 'soliloquy-crop', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

        /** Setup the license checker */
        global $soliloquy_license;

        /** Only process update if a key has been entered and updates are on */
        if ( is_admin() ) :
            if ( isset( $soliloquy_license['license'] ) ) {
                $args = array(
                    'remote_url'    => 'http://soliloquywp.com/',
                    'version'       => $this->version,
                    'plugin_name'   => 'Soliloquy Crop Addon',
                    'plugin_slug'   => 'soliloquy-crop',
                    'plugin_path'   => plugin_basename( __FILE__ ),
                    'plugin_url'    => WP_PLUGIN_URL . '/soliloquy-crop',
                    'time'          => 43200,
                    'key'           => $soliloquy_license['license']
                );

                /** Instantiate the automatic plugin updater class */
                $tgmsp_crop_updater = new Tgmsp_Updater( $args );
            }

            // Instantiate all necessary admin components of the plugin.
            $tgmsp_crop_admin   = new Tgmsp_Crop_Admin();
            $tgmsp_crop_assets  = new Tgmsp_Crop_Assets();

            // If the Preview Addon is available, load the Preview class.
            if ( class_exists( 'Tgmsp_Preview', false ) ) // Don't check the autoload stack for this instantiation
                $tgmsp_crop_preview = new Tgmsp_Crop_Preview();
        endif;

        /** Instantiate all the necessary components of the plugin */
        $tgmsp_crop_shortcode   = new Tgmsp_Crop_Shortcode();
        $tgmsp_crop_strings     = new Tgmsp_Crop_Strings();

    }

    /**
     * PSR-0 compliant autoloader to load classes as needed.
     *
     * @since 1.0.0
     *
     * @param string $classname The name of the class
     * @return null Return early if the class name does not start with the correct prefix
     */
    public static function autoload( $classname ) {

        if ( 'Tgmsp_Crop' !== mb_substr( $classname, 0, 10 ) )
            return;

        $filename = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . str_replace( '_', DIRECTORY_SEPARATOR, $classname ) . '.php';
        if ( file_exists( $filename ) )
            require $filename;

    }

    /**
     * API function for cropping images.
     *
     * @since 1.0.3
     *
     * @global object $wpdb The $wpdb database object.
     *
     * @param string $url The URL of the image to resize.
     * @param int $width The width for cropping the image.
     * @param int $height The height for cropping the image.
     * @param bool $crop Whether or not to crop the image (default yes).
     * @param string $align The crop position alignment.
     * @param bool $retina Whether or not to make a retina copy of image.
     * @return WP_Error|string Return WP_Error on error, URL of resized image on success.
     */
    public static function resize_image( $url, $width = null, $height = null, $crop = true, $align = 'c', $quality = 100, $retina = false ) {

        global $wpdb;

        // Get common vars.
        $args   = func_get_args();
        $common = self::get_image_info( $args );

        // Unpack variables if an array, otherwise return WP_Error.
        if ( is_wp_error( $common ) )
            return $common;
        else
            extract( $common );

        // If the file doesn't exist yet, we need to create it.
        if ( ! file_exists( $dest_file_name ) ) {
            // We only want to resize Media Library images, so we can be sure they get deleted correctly when appropriate.
            $get_attachment = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE guid='%s'", $url ) );

            // Load the WordPress image editor.
            $editor = wp_get_image_editor( $file_path );

            // If an editor cannot be found, the user needs to have GD or Imagick installed.
            if ( is_wp_error( $editor ) )
                return new WP_Error( 'soliloquy-crop-error-no-editor', Tgmsp_Crop_Strings::get_instance()->strings['error_no_editor'] );

            // Set the image editor quality.
            $editor->set_quality( $quality );

            // If cropping, process cropping.
            if ( $crop ) {
                $src_x = $src_y = 0;
                $src_w = $orig_width;
                $src_h = $orig_height;

                $cmp_x = $orig_width / $dest_width;
                $cmp_y = $orig_height / $dest_height;

                // Calculate x or y coordinate and width or height of source
                if ( $cmp_x > $cmp_y ) {
                    $src_w = round( $orig_width / $cmp_x * $cmp_y );
                    $src_x = round( ($orig_width - ($orig_width / $cmp_x * $cmp_y)) / 2 );
                } else if ( $cmp_y > $cmp_x ) {
                    $src_h = round( $orig_height / $cmp_y * $cmp_x );
                    $src_y = round( ($orig_height - ($orig_height / $cmp_y * $cmp_x)) / 2 );
                }

                // Positional cropping.
                if ( $align && $align != 'c' ) {
                    if ( strpos( $align, 't' ) !== false || strpos( $align, 'tr' ) !== false || strpos( $align, 'tl' ) !== false ) {
                        $src_y = 0;
                    }

                    if ( strpos( $align, 'b' ) !== false || strpos( $align, 'br' ) !== false || strpos( $align, 'bl' ) !== false ) {
                        $src_y = $orig_height - $src_h;
                    }

                    if ( strpos( $align, 'l' ) !== false ) {
                        $src_x = 0;
                    }

                    if ( strpos ( $align, 'r' ) !== false ) {
                        $src_x = $orig_width - $src_w;
                    }
                }

                // Crop the image.
                $editor->crop( $src_x, $src_y, $src_w, $src_h, $dest_width, $dest_height );
            } else {
                // Just resize the image.
                $editor->resize( $dest_width, $dest_height );
            }

            // Save the image.
            $saved = $editor->save( $dest_file_name );

            // Print possible out of memory errors.
            if ( is_wp_error( $saved ) ) {
                @unlink($dest_file_name);
                return $saved;
            }

            // Add the resized dimensions and alignment to original image metadata, so the images
            // can be deleted when the original image is delete from the Media Library.
            if ( $get_attachment ) {
                $metadata = wp_get_attachment_metadata( $get_attachment[0]->ID );

                if ( isset( $metadata['image_meta'] ) ) {
                    $md = $saved['width'] . 'x' . $saved['height'];

                    if ( $crop )
                        $md .= ($align) ? "_${align}" : "_c";

                    $metadata['image_meta']['resized_images'][] = $md;
                    wp_update_attachment_metadata( $get_attachment[0]->ID, $metadata );
                }
            }

            // Set the resized image URL.
            $resized_url = str_replace( basename( $url ), basename( $saved['path'] ), $url );
        } else {
            // Set the resized image URL.
            $resized_url = str_replace( basename( $url ), basename( $dest_file_name ), $url );
        }

        // Return the resized image URL.
        return $resized_url;

    }

    /**
     * Helper function to return common information about an image.
     *
     * @since 1.0.3
     *
     * @param array $args List of resizing args to expand for gathering info.
     * @return WP_Error|string Return WP_Error on error, array of data on success.
     */
    public static function get_image_info( $args ) {

        // Unpack arguments.
        list( $url, $width, $height, $crop, $align, $quality, $retina ) = $args;

        // Return an error if no URL is present.
        if ( empty( $url ) )
            return new WP_Error( 'soliloquy-crop-error-no-url', Tgmsp_Crop_Strings::get_instance()->strings['error_no_url'] );

        // Get the image file path.
        $urlinfo       = parse_url( $url );
        $wp_upload_dir = wp_upload_dir();

        // Interpret the file path of the image.
        if ( preg_match('/\/[0-9]{4}\/[0-9]{2}\/.+$/', $urlinfo['path'], $matches ) ) {
            $file_path = $wp_upload_dir['basedir'] . $matches[0];
        } else {
            $pathinfo    = parse_url( $url );
            $uploads_dir = is_multisite() ? '/files/' : '/wp-content/';
            $file_path   = ABSPATH . str_replace( dirname( $_SERVER['SCRIPT_NAME'] ) . '/', '', strstr( $pathinfo['path'], $uploads_dir ) );
            $file_path   = preg_replace( '/(\/\/)/', '/', $file_path );
        }

        // Don't process a file that does not exist.
        if ( ! file_exists( $file_path ) ) {
            return new WP_Error( 'soliloquy-crop-error-no-file', Tgmsp_Crop_Strings::get_instance()->strings['error_no_file'] );
        }

        // Get original image size
        $size = is_user_logged_in() ? getimagesize( $file_path ) : @getimagesize( $file_path );

        // If no size data obtained, return an error.
        if ( ! $size )
            return new WP_Error( 'soliloquy-crop-error-no-size', Tgmsp_Crop_Strings::get_instance()->strings['error_no_size'] );

        // Set original width and height.
        list( $orig_width, $orig_height, $orig_type ) = $size;

        // Generate width or height if not provided.
        if ( $width && ! $height ) {
            $height = floor( $orig_height * ($width / $orig_width) );
        } else if ( $height && ! $width ) {
            $width = floor( $orig_width * ($height / $orig_height) );
        } else if ( ! $width && ! $height ) {
            return new WP_Error( 'soliloquy-crop-error-no-size', Tgmsp_Crop_Strings::get_instance()->strings['error_no_size'] );
        }

        // Allow for different retina image sizes.
        $retina = $retina ? ( $retina === true ? 2 : $retina ) : 1;

        // Destination width and height variables
        $dest_width  = $width * $retina;
        $dest_height = $height * $retina;

        // Some additional info about the image.
        $info = pathinfo( $file_path );
        $dir  = $info['dirname'];
        $ext  = $info['extension'];
        $name = wp_basename( $file_path, ".$ext" );

        // Suffix applied to filename
        $suffix = "${dest_width}x${dest_height}";

        // Set alignment information on the file.
        if ( $crop )
            $suffix .= ( $align ) ? "_${align}" : "_c";

        // Get the destination file name
        $dest_file_name = "${dir}/${name}-${suffix}.${ext}";

        // Return the info.
        return array(
            'dir'            => $dir,
            'name'           => $name,
            'ext'            => $ext,
            'suffix'         => $suffix,
            'orig_width'     => $orig_width,
            'orig_height'    => $orig_height,
            'orig_type'      => $orig_type,
            'dest_width'     => $dest_width,
            'dest_height'    => $dest_height,
            'file_path'      => $file_path,
            'dest_file_name' => $dest_file_name,
        );

    }

    /**
     * Helper method to determine if Soliloquy is inactive or not.
     *
     * @since 1.0.0
     *
     * @global string $pagenow The current page slug
     * @return bool True if Soliloquy is not active, false otherwise
     */
    public static function soliloquy_is_not_active() {

        global $pagenow;

        return ! ( class_exists( 'Tgmsp' ) ) && ! ( isset( $_GET['action'] ) && 'do-plugin-upgrade' == $_GET['action'] || 'plugin-editor.php' == $pagenow && isset( $_REQUEST['file'] ) && preg_match( '|^soliloquy|', $_REQUEST['file'] ) || 'update-core.php' == $pagenow );

    }

    /**
     * Getter method for retrieving the object instance.
     *
     * @since 1.0.0
     */
    public static function get_instance() {

        return self::$instance;

    }

    /**
     * Getter method for retrieving the main plugin filepath.
     *
     * @since 1.0.0
     */
    public static function get_file() {

        return self::$file;

    }

}

/** Instantiate the init class */
$tgmsp_crop = new Tgmsp_Crop();
<?php
/**
 * Strings class for the Soliloquy Featured Content Addon.
 *
 * @since 1.0.0
 *
 * @package	Soliloquy Featured Content
 * @author	Thomas Griffin
 */
class Tgmsp_FC_Strings {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Holds a copy of all the strings used by the Soliloquy Featured Content Addon.
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	public $strings = array();

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		self::$instance = $this;

		/** Return early if Soliloquy is not active */
		if ( Tgmsp_FC::soliloquy_is_not_active() )
			return;

		$this->strings = apply_filters( 'tgmsp_fc_strings', array(
			'asc'						=> __( 'Ascending Order', 'soliloquy-featured-content' ),
			'author'					=> __( 'Author', 'soliloquy-featured-content' ),
			'comments'					=> __( 'Comments', 'soliloquy-featured-content' ),
			'content'					=> __( 'Content Settings', 'soliloquy-featured-content' ),
			'content_type'              => __( 'Select the type of content to display in the slider:', 'soliloquy-featured-content' ),
			'date'						=> __( 'Date', 'soliloquy-featured-content' ),
			'desc'						=> __( 'Descending Order', 'soliloquy-featured-content' ),
			'ellipses'					=> __( 'Append an ellipses to the post content?', 'soliloquy-featured-content' ),
			'ellipses_desc'				=> __( 'Places an ellipses after the last word of the post content.', 'soliloquy-featured-content' ),
			'exclude'					=> __( 'Exclude', 'soliloquy-featured-content' ),
			'fallback'					=> __( 'Specify a fallback image URL if no image is found:', 'soliloquy-featured-content' ),
			'fallback_desc'				=> __( 'Used if no images can be found.', 'soliloquy-featured-content' ),
			'featured_label'			=> __( 'Featured Content Slider', 'soliloquy-featured-content' ),
			'id'						=> __( 'ID', 'soliloquy-featured-content' ),
			'include'					=> __( 'Include', 'soliloquy-featured-content' ),
			'intro'						=> __( 'The Featured Content Slider will use images that have been set as a featured image in your posts, pages or custom post types. If you have not set featured images for the content type you want to include, consider using the Default Slider option instead.', 'soliloquy-featured-content' ),
			'menu_order'				=> __( 'Menu Order', 'soliloquy-featured-content' ),
			'modified_date'				=> __( 'Modified Date', 'soliloquy-featured-content' ),
			'no_content'                => __( 'No Content', 'soliloquy-featured-content' ),
			'post_content'				=> __( 'Post Content', 'soliloquy-featured-content' ),
			'post_excerpt'              => __( 'Post Excerpt', 'soliloquy-featured-content' ),
			'post_content_length' 		=> __( 'Set the number of words to display:', 'soliloquy-featured-content' ),
			'post_content_length_desc' 	=> __( 'Defaults to 40 words.', 'soliloquy-featured-content' ),
			'post_slug'					=> __( 'Post Slug', 'soliloquy-featured-content' ),
			'post_title'				=> __( 'Display the post title?', 'soliloquy-featured-content' ),
			'post_title_desc'			=> __( 'If unchecked, no post title will be displayed for the image.', 'soliloquy-featured-content' ),
			'post_title_link'			=> __( 'Link post title to the post URL?', 'soliloquy-featured-content' ),
			'post_title_link_desc'		=> __( 'If unchecked, the post title will not be linked to a URL.', 'soliloquy-featured-content' ),
			'post_url'					=> __( 'Link image to the post URL?', 'soliloquy-featured-content' ),
			'post_url_desc'				=> __( 'If unchecked, the image will not be linked to a URL.', 'soliloquy-featured-content' ),
			'query'						=> __( 'Query Settings', 'soliloquy-featured-content' ),
			'random'					=> __( 'Random', 'soliloquy-featured-content' ),
			'read_more'					=> __( 'Display a read more link?', 'soliloquy-featured-content' ),
			'read_more_desc'			=> __( 'If unchecked, no read more link will be displayed.', 'soliloquy-featured-content' ),
			'read_more_default'			=> __( 'Continue Reading...', 'soliloquy-featured-content' ),
			'read_more_text'			=> __( 'Set the read more link text:', 'soliloquy-featured-content' ),
			'read_more_text_desc'		=> __( 'Defaults to "Continue Reading..."', 'soliloquy-featured-content' ),
			'step_one'					=> __( 'Select your post type (or multiple post types if you prefer):', 'soliloquy-featured-content' ),
			'step_one_hold'				=> esc_attr__( 'Select post types to query (defaults to post)…', 'soliloquy-featured-content' ),
			'step_two'					=> __( 'Choose a term or terms to determine what content is included:', 'soliloquy-featured-content' ),
			'step_two_hold'				=> esc_attr__( 'Select term or terms (defaults to none)…', 'soliloquy-featured-content' ),
			'step_three'				=> __( 'Let\'s %s ONLY the following items:', 'soliloquy-featured-content' ),
			'step_three_hold'			=> esc_attr__( 'Make your selection (defaults to none)…', 'soliloquy-featured-content' ),
			'step_four'					=> __( 'Let\'s sort these posts by:', 'soliloquy-featured-content' ),
			'step_five'					=> __( 'Let\'s also order these posts by:', 'soliloquy-featured-content' ),
			'step_six'					=> __( 'Enter the maximum number of slides for the slider:', 'soliloquy-featured-content' ),
			'step_seven'				=> __( 'Enter the number of posts to offset:', 'soliloquy-featured-content' ),
			'step_eight'				=> __( 'Select the post status for the posts:', 'soliloquy-featured-content' ),
			'title'						=> __( 'Title', 'soliloquy-featured-content' )
		) );

	}

	/**
	 * Getter method for retrieving the object instance.
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {

		return self::$instance;

	}

}